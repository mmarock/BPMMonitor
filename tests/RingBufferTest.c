

#include "RingBuffer.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <time.h>

#define TEST_SIZE 5000l

static void fill(uint16_t *vals)
{

	for (int i = 0; i < TEST_SIZE; ++i) {
		vals[i] = rand();
	}
}

static void test_push(void)
{

	printf("TestPush()\n");
	struct RingBuffer rb;
	RingBufferInit(&rb);

	uint16_t test_values[TEST_SIZE];
	fill(test_values);

	int i = 0;
	while (i < TEST_SIZE && RingBufferPush(&rb, test_values + i)) {
		RingBufferPrint(&rb);
		++i;
	}

	for (int j = 0; j < RINGBUFFER_SIZE && j < TEST_SIZE; ++j) {
		printf("%d: %d, %d\n", j, test_values[j], rb.data[j]);
	}
	for (int j = 0; j < RINGBUFFER_SIZE && j < TEST_SIZE; ++j) {
		assert(test_values[j] == rb.data[j]);
	}
}

static void test_pop(void)
{

	printf("TestPop()\n");
	struct RingBuffer rb;
	RingBufferInit(&rb);

	uint16_t test_values[TEST_SIZE];
	fill(test_values);

	uint16_t result[TEST_SIZE];

	int i = 0;
	int k = 0;
	while (i < TEST_SIZE) {
		printf("TestPop(): new load\n");
		assert(k < TEST_SIZE);

		while (RingBufferPush(&rb, &test_values[i])) {
			RingBufferPrint(&rb);
			++i;
		}

		while (RingBufferPop(&rb, result + k)) {
			RingBufferPrint(&rb);
			assert(test_values[k] == result[k]);
			++k;
		}
	}

	for (int j = 0; j < RINGBUFFER_SIZE && j < TEST_SIZE; ++j) {
		printf("%d: %d, %d\n", j, test_values[j], result[j]);
		assert(test_values[j] == result[j]);
	}
}

static void test_continous_stream(void)
{

	printf("Now the last test\n");

	struct RingBuffer rb;
	RingBufferInit(&rb);

	uint16_t test_values[TEST_SIZE];
	fill(test_values);

	int counter = 0;
	uint16_t result[TEST_SIZE];

	assert(!RingBufferSize(&rb) && "empty ringbuffer must have size = 0");

	for (int i = 0; i < TEST_SIZE; ++i) {

		if (RingBufferPush(&rb, test_values + i)) {
			assert(i < TEST_SIZE);
			// printf("pushed %d\n", test_values[i]);
			continue;
		}

		while (RingBufferPop(&rb, result + counter)) {
			assert(counter < TEST_SIZE);
			// printf("popped %d\n", result[counter]);
			counter++;
		}

		RingBufferPush(&rb, test_values + i);
		// printf("pushed %d\n", test_values[i]);
		assert(counter < TEST_SIZE);
	}

	while (RingBufferPop(&rb, result + counter)) {
		// printf("popped %d\n", result[counter]);
		counter++;
	}
	assert(!RingBufferSize(&rb) && "empty ringbuffer must have size = 0");

	for (int i = 0; i < TEST_SIZE; ++i) {
		printf("%d - %d, %d\t%s\n", i, test_values[i], result[i],
		       (test_values[i] == result[i]) ? "" : "SHIT HAPPENED");
		assert(test_values[i] == result[i]);
	}
}

int main()
{

	test_push();

	test_pop();

	test_continous_stream();

	printf("Done - nothing happened\n");

	return 0;
}
