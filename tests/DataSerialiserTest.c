

#include "DataSerialiser.h"

#include <stdio.h>
#include <stdlib.h>

#define TEST_SIZE 7500

static void fill(uint16_t *vals)
{
	for (int i = 0; i < TEST_SIZE; ++i) {
		vals[i] = rand();
	}
}

int main(void)
{

	uint16_t test_vals[TEST_SIZE];
	fill(test_vals);

	struct DataSerialiser ds;
	DataSerialiserInit(&ds);

	int counter = 0;
	for (int i = 0; i < TEST_SIZE; ++i) {
		printf("Habe eine Zahl: %d\n", test_vals[i]);
		if (!DataSerialiserPushInt(&ds, test_vals + i)) {
			DataSerialiserPushChar(&ds, '\x04');
			printf("Buffer voll, reinit.\n");
			printf("Inhalt: %.*s\n",
			       (int)(DataSerialiserEnd(&ds) -
				     DataSerialiserBegin(&ds)),
			       DataSerialiserBegin(&ds));
			DataSerialiserInit(&ds);
			++counter;
		}
		DataSerialiserPrint(&ds);
	}
	printf("Ich habe für %d Werte den Buffer %d mal leeren müssen\n",
	       TEST_SIZE, counter);
	return 0;
}
