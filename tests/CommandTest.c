
#include "Command.h"
#include <stdio.h>

#define LEN 10

#include <stdint.h>

int main(void)
{

	uint8_t buffer[] = { '\x02',
			     0,
			     CMD_MOTOR_START,
			     '\x03',
			     '\x02',
			     0, // Wrong command
			     0,
			     CMD_MEASURMENT_START,
			     '\x03',
			     '\x02',
			     0,
			     CMD_MEASURMENT_START,
			     '\x03',
			     '\x02',
			     0,
			     CMD_MEASURMENT_STOP,
			     '\x03' };

	struct Command cmd[LEN];

	int8_t c = CommandParse(NULL, buffer, sizeof(buffer), cmd, LEN);

	printf("Parse: c:%d\n", c);
	for (int8_t i = 0; i < c; ++i) {
		printf("\t->cmd[%d]: %c\n", i, cmd[i].type);
	}
	return 0;
}
