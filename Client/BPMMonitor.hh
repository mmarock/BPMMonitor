

#ifndef BPMMONITOR_HH_INCLUDED
#define BPMMONITOR_HH_INCLUDED

#include <memory>
#include <string>
#include <vector>

#include <cstdint>

namespace bpmmonitor
{

////////////////////////////////////////////////////////////
//	class BPMMonitorException
////////////////////////////////////////////////////////////
class BPMMonitorException : public std::exception
{
	std::string msg_;

public:
	BPMMonitorException(const char *msg);
	BPMMonitorException(const std::string &msg);

	virtual const char *what() const noexcept final;
};

////////////////////////////////////////////////////////////
//	class BPMMonitor
////////////////////////////////////////////////////////////
class BPMMonitor
{
	// ugly stuf, encapsulated
	class Impl;
	std::unique_ptr<Impl> impl_;

public:
	// hostname to ip, port is fixed inside class
	BPMMonitor(const std::string &hostname);
	~BPMMonitor();

	// no copies
	BPMMonitor(const BPMMonitor &rhs) = delete;
	BPMMonitor &operator=(const BPMMonitor &rhs) = delete;

	// move semantic
	BPMMonitor(BPMMonitor &&rhs);
	BPMMonitor &operator=(BPMMonitor &&rhs);

	void setTimeoutInMs(int timeout) noexcept;
	const int &getTimeoutInMs() const;

	std::vector<uint16_t> frame();

	void start();
	void stop();
	bool running() const;
};

} // namespace bpmmonitor

#endif // BPMMONITOR_HH_INCLUDED
