
#include "BPMMonitor.hh"

#include <algorithm>
#include <iostream>

#include <cassert>
#include <cstdlib>
#include <cstring>

#include <boost/asio.hpp>

namespace bpmmonitor
{

////////////////////////////////////////////////////////////
//	class BPMMonitorException implementation
////////////////////////////////////////////////////////////

BPMMonitorException::BPMMonitorException(const char *msg)
    : msg_(std::string("BPMMonitorException: ") + msg)
{
}

BPMMonitorException::BPMMonitorException(const std::string &msg)
    : msg_(std::string("BPMMonitorException: ") + msg)
{
}

const char *BPMMonitorException::what() const noexcept { return msg_.c_str(); }

////////////////////////////////////////////////////////////
//	BPMMonitorException children
////////////////////////////////////////////////////////////

struct BPMMonitorErrorCode : public BPMMonitorException {
	BPMMonitorErrorCode(uint16_t state_no)
	    : BPMMonitorException(
		  std::string("BPMMonitorErrorCode: failed with code:") +
		  std::to_string(state_no))
	{
	}
};

struct BPMMonitorTimeout : public BPMMonitorException {
	explicit BPMMonitorTimeout() : BPMMonitorException("timeout") {}
};

struct BPMMonitorDisconnected : public BPMMonitorException {
	explicit BPMMonitorDisconnected()
	    : BPMMonitorException("server side disconnect")
	{
	}
};

struct AsioException : public BPMMonitorException {
	template <class BoostT>
	AsioException(const BoostT &ex)
	    : BPMMonitorException(std::string("") + ex.what())
	{
	}
};

////////////////////////////////////////////////////////////
//	class BPMMonitor::Impl
////////////////////////////////////////////////////////////

class BPMMonitor::Impl
{
public:
	enum class KEY {
		CLOSE,
		FRAME,
		MOTOR_ON,
		MOTOR_OFF,
		MOTOR_STATE,
		REPLY_OK,
		REPLY_ERROR
	};
	static const char *key_set(enum KEY key);

	static const uint8_t *find(const uint8_t *it, const uint8_t *const end,
				   const uint8_t &what);

	struct Reply {
		enum HEADER : uint8_t { NOT_PARSED, ERROR, OK } header_type;
		uint16_t header_no;
		std::vector<uint16_t> data;
		explicit Reply();
	};
	static void parse_line(const uint8_t *begin, const uint8_t *const end,
			       Reply &out);
	static uint16_t get_number(const uint8_t *begin,
				   const uint8_t *const end);

private:
	using Socket = boost::asio::ip::tcp::socket;

	std::string hostname_;
	mutable Socket socket_;
	int timeout_in_ms_;

public:
	// hostname to ip, port is fixed inside class
	Impl(const std::string &hostname);
	~Impl();

	// no copies
	Impl(const Impl &rhs) = delete;
	Impl &operator=(const Impl &rhs) = delete;

	void setTimeoutInMs(int timeout) noexcept;
	const int &getTimeoutInMs() const;

	std::vector<uint16_t> frame();
	void start();
	void stop();
	bool running() const;

	// send cmds
	void sendPayload(const KEY &payload) const;
	struct Reply waitForReply() const;

private:
	static boost::asio::io_service &get_io_service()
	{
		static boost::asio::io_service service;
		return service;
	}

	static Socket resolve_and_connect(const std::string &hostname)
	{

		using namespace boost::asio;

		auto &io = get_io_service();
		ip::tcp::resolver resolver(io);

		ip::tcp::resolver::query query(hostname, "7500");

		auto it = resolver.resolve(query);
		auto end = ip::tcp::resolver::iterator();

		Socket s(io);
		boost::system::error_code error;
		while (it != end) {

			s.connect(*it++, error);
			if (error) {
				s.close();
				continue;
			}
			return s;
		}
		throw BPMMonitorException("no suitable connection established");
	}
};

const char *BPMMonitor::Impl::key_set(enum KEY key)
{
	switch (key) {
	case KEY::CLOSE:
		return "CLOSE\n";
	case KEY::FRAME:
		return "FRAME\n";
	case KEY::MOTOR_ON:
		return "MOTOR 1\n";
	case KEY::MOTOR_OFF:
		return "MOTOR 0\n";
	case KEY::MOTOR_STATE:
		return "MOTOR ?\n";
	case KEY::REPLY_OK:
		return "OK\t";
	case KEY::REPLY_ERROR:
		return "ERR\t";
	};
}
BPMMonitor::Impl::Impl(const std::string &hostname)
    : hostname_(hostname), socket_(resolve_and_connect(hostname)),
      timeout_in_ms_(2000)
{
}

BPMMonitor::Impl::~Impl() {}

void BPMMonitor::Impl::setTimeoutInMs(int timeout) noexcept
{
	timeout_in_ms_ = timeout;
}

const int &BPMMonitor::Impl::getTimeoutInMs() const { return timeout_in_ms_; }

std::vector<uint16_t> BPMMonitor::Impl::frame()
{
	sendPayload(KEY::FRAME);
	Reply r = waitForReply();
	if (r.header_type != Reply::OK) {
		throw BPMMonitorErrorCode(r.header_no);
	}
	assert(r.data.size() == r.header_no &&
	       "header_no and vector not the same size!");
	return r.data;
}

void BPMMonitor::Impl::start()
{
	sendPayload(KEY::MOTOR_ON);
	Reply r = waitForReply();
	if (r.header_type != Reply::OK) {
		throw BPMMonitorErrorCode(r.header_no);
	}
}

void BPMMonitor::Impl::stop()
{
	sendPayload(KEY::MOTOR_OFF);
	Reply r = waitForReply();
	if (r.header_type != Reply::OK) {
		throw BPMMonitorErrorCode(r.header_no);
	}
}

bool BPMMonitor::Impl::running() const
{
	sendPayload(KEY::MOTOR_STATE);
	Reply r = waitForReply();
	if (r.header_type != Reply::OK) {
		throw BPMMonitorErrorCode(r.header_no);
	}
	return r.data.at(0) == 1;
}

void BPMMonitor::Impl::sendPayload(const KEY &key) const
{
	const char *payload = key_set(key);
	ssize_t payload_len = strlen(payload);
	std::clog << "BPMMonitor: try send: \"" << payload << "\"" << std::endl;
	try {
		boost::asio::write(socket_,
				   boost::asio::buffer(payload, payload_len));
	} catch (const boost::system::system_error &ex) {
		throw AsioException(ex);
	}
}

BPMMonitor::Impl::Reply::Reply() : header_type(Reply::NOT_PARSED), header_no(0)
{
}

const uint8_t *BPMMonitor::Impl::find(const uint8_t *it,
				      const uint8_t *const end,
				      const uint8_t &what)
{
	assert(it && "is NULL");
	assert(end && "is NULL");
	// find newline
	while (it < end && *it != what) {
		++it;
	}
	// found? return ptr, else nullptr
	return (it < end) ? it : nullptr;
}

void BPMMonitor::Impl::parse_line(const uint8_t *it, const uint8_t *const end,
				  Reply &out)
{
	assert(it && "is NULL");
	assert(end && "is NULL");
	if (out.header_type == Reply::NOT_PARSED) {
		// look for first line
		const char *str = key_set(KEY::REPLY_OK);
		size_t len = strlen(str);
		if (!memcmp(str, it, len)) {
			it += len;
			out.header_type = Reply::OK;
			out.header_no = get_number(it, end);
			return;
		}
		str = key_set(KEY::REPLY_ERROR);
		len = strlen(str);
		if (!memcmp(str, it, len)) {
			it += len;
			out.header_type = Reply::ERROR;
			out.header_no = get_number(it, end);
			return;
		}
		throw BPMMonitorException("stream parsing failed");
	}
	// look for data line
	out.data.push_back(get_number(it, end));
	std::cout << "Value added: " << out.data.back()
		  << "; Vector size now: " << out.data.size() << std::endl;
}

uint16_t BPMMonitor::Impl::get_number(const uint8_t *begin,
				      const uint8_t *const end)
{
	assert(begin && "is NULL");
	assert(end && "is NULL");
	assert(end - begin > 0 && "end is less than begin");
	std::cout << "Try Parse Number:"
		  << std::string((const char *)begin, end - begin) << std::endl;
	char *endptr = nullptr;
	uint16_t r = strtoul(reinterpret_cast<const char *>(begin), &endptr, 0);
	if (endptr == reinterpret_cast<const char *>(begin)) {
		throw BPMMonitorException("conversion: no digit found");
	}
	std::cout << "Got number:" << r << std::endl;
	return r;
}

struct BPMMonitor::Impl::Reply BPMMonitor::Impl::waitForReply() const
{
	uint8_t buffer[128];
	ssize_t written(0), ret(0);
	bool keep_receiving(true);
	Reply r;
	while (keep_receiving) {

		// overflow, one cmd was much longer than expected
		if (written >= static_cast<ssize_t>(sizeof(buffer))) {
			written = 0;
		}

		try {
			ret = socket_.receive(boost::asio::buffer(
			    buffer + written, sizeof(buffer) - written));
			std::clog
			    << "BPMMonitor: received " << ret << " bytes ("
			    << std::string((const char *)buffer, ret) << ")"
			    << std::endl;
			if (!ret) {
				throw BPMMonitorDisconnected();
			}
		} catch (const boost::system::system_error &ex) {
			throw AsioException(ex);
		}
		// actually got something
		written += ret;

		// look for newline
		const uint8_t *start = buffer;
		const uint8_t *const end = buffer + written;
		const uint8_t *newline = find(buffer, end, '\n');
		while ((newline = find(start, end, '\n'))) {
			parse_line(start, newline, r);
			start = newline + 1;
		}
		// everything from start until end -> move to the beginning
		written = end - start;
		memmove(buffer, start, written);

		if ((r.header_type == Reply::OK &&
		     r.data.size() == r.header_no) ||
		    (r.header_type == Reply::ERROR)) {
			keep_receiving = false;
		}
	}
	std::cout << "WaitForReply: Have " << r.data.size() << " vals in data"
		  << std::endl;

	if (!ret) {
		throw BPMMonitorTimeout();
	}

	return r;
}

////////////////////////////////////////////////////////////
//	class BPMMonitor implementation
////////////////////////////////////////////////////////////

BPMMonitor::BPMMonitor(const std::string &hostname)
    : impl_(new BPMMonitor::Impl(hostname))
{
}

BPMMonitor::~BPMMonitor() {}

BPMMonitor::BPMMonitor(BPMMonitor &&rhs)
    : impl_(std::forward<std::unique_ptr<BPMMonitor::Impl>>(rhs.impl_))
{
}

BPMMonitor &BPMMonitor::operator=(BPMMonitor &&rhs)
{
	if (this != &rhs) {
		this->impl_ =
		    std::forward<std::unique_ptr<BPMMonitor::Impl>>(rhs.impl_);
	}
	return *this;
}

void BPMMonitor::setTimeoutInMs(int timeout) noexcept
{
	assert(impl_ && "impl is NULL");
	impl_->setTimeoutInMs(timeout);
}

const int &BPMMonitor::getTimeoutInMs() const
{
	assert(impl_ && "impl is NULL");
	return impl_->getTimeoutInMs();
}

std::vector<uint16_t> BPMMonitor::frame()
{
	assert(impl_ && "impl is NULL");
	return impl_->frame();
}

void BPMMonitor::start()
{
	assert(impl_ && "impl is NULL");
	impl_->start();
}

void BPMMonitor::stop()
{
	assert(impl_ && "impl is NULL");
	impl_->stop();
}

bool BPMMonitor::running() const
{
	assert(impl_ && "impl is NULL");
	return impl_->running();
}

} // namespace bpmmonitor
