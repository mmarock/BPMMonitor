

BPMMonitor
==========
Read a Beam Profile Monitor DataSet with an Arduino Leonardo Eth
and make these data public in a network.

Description
-----------
This software is written for an Arduino Leonardo Eth.
You have to compile and transmit this software to the microcontroller (atmega32u4).
I used the Atmel [DFU-Bootloader](http://www.atmel.com/Images/doc7618.pdf) (2kB in size)
because I wanted the additional space, the Arduino bootloader needed 4kB.
On Linux there is a tool dfu-programmer for this type of bootloader,
but the avrdude should work too. Libc is avr-libc for atmel mucs.

License
-------
look [here](LICENSE)

See Also
--------
* [DRV10983 datasheet](http://www.ti.com/lit/ds/symlink/drv10983.pdf)
* [avr-libc page](http://www.nongnu.org/avr-libc/)
* [W5500 datasheet](https://cdn.sparkfun.com/datasheets/Dev/Arduino/Shields/W5500_datasheet_v1.0.2_1.pdf)
* [atmega32u4 datasheet](http://www.atmel.com/Images/Atmel-7766-8-bit-AVR-ATmega16U4-32U4_Datasheet.pdf)
* [arduino leonardo eth schematic](http://download.arduino.org/products/LEONARDOETH/Arduino_Leonardo_Eth_V1_sch.pdf)
