


#ifndef QBPMMONITOR_HH_INCLUDED
#define QBPMMONITOR_HH_INCLUDED

#include <QObject>
#include <QString>
#include "../../src/BPMMonitor.hh"

namespace bpmmonitor {

	/** 
	 * @brief Ein wrapper objekt für eine einfachere Eingliederung in qt
	 *
	 * Ein paar notes:
	 * - QThreads sind keine normalen Threads
	 * - das Event System ist nicht exceptions safe
	 * - slot/signals funktionieren über QThreads hinaus
	 * - QThreads können ein QObject bekommen, 
	 *   	events werden dann im thread abgearbeitet was ich will
	 * - Das Beispiel impl. mindestens einen slot (doWork())
	 *   	und ein signal (isReady), welchher das Resultat übergibt
	 *   	und vom Worker emittiert wird.
	 *
	 */
class QBPMMonitor : public QObject {

	Q_OBJECT

	BPMMonitor monitor_;

public:
	QBPMMonitor(QString hostname, QObject *parent = 0);

public slots:
	void exec();

signals:
	void resultReady();



};

} // namespace bpmmonitor


#endif // QBPMMONITOR_HH_INCLUDED
