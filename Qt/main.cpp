

#include "TestControl.hh"
#include <QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	try {
		TestControl w;
		w.show();
		return a.exec();
	} catch (const std::exception &ex) {
		return -1;
	}
}
