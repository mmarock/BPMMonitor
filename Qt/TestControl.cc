

#include "TestControl.hh"

#include <cassert>

#include <QFile>
#include <QInputDialog>
#include <QMessageBox>
#include <QTextStream>
#include <QtCharts/QChartView>

#include <QFileDialog>

TestControl::TestControl(QMainWindow *parent)
    : QMainWindow(parent), bpm_(TestControl::make_bpm_monitor())
{
	setupUi(this);
	setupBPMInterface();
	setupChart();
}

TestControl::~TestControl() {}

void TestControl::setupBPMInterface(void)
{
	// connect calls
	// on button press
	connect(pushButton, &QPushButton::pressed, this, &TestControl::send_command_to_bpm);
	// on auto_updater changed
	connect(auto_updater, &QCheckBox::stateChanged, [&](int result) {
		if (result) {
			bpm_.start();
			timer_.start(100);
		} else {
			timer_.stop();
			bpm_.stop();
		}
	});

	timer_.moveToThread(&timer_thr_);
	// timer timeout
	connect(&timer_, &QTimer::timeout, this, &TestControl::get_new_data);
	timer_thr_.start();

	// connect view on click and data write
	connect(snapshot, &QPushButton::pressed, [&](void) {
		timer_.stop();
		do_a_snapshot();
		timer_.start(100);
	});
}

void TestControl::setupChart(void)
{
	chart_.addSeries(&data_lines_);
	chart_.setTitle("Erhaltene Daten");
	chart_.createDefaultAxes();
	chart_.axes(Qt::Horizontal).back()->setRange(0, 300);
	chart_.axes(Qt::Vertical).back()->setRange(0, 1024);
	auto *view = new QtCharts::QChartView(&chart_);
	view->setRenderHint(QPainter::Antialiasing);
	tabWidget->addTab(view, "DataViewer");
}

bpmmonitor::BPMMonitor TestControl::make_bpm_monitor(void)
{
	using namespace bpmmonitor;
	try {
		return BPMMonitor(QInputDialog::getText(
				      nullptr,
				      QString("Enter the BPMMonitor hostname"),
				      QString("hostname:"))
				      .toStdString());
	} catch (const BPMMonitorException &ex) {
		QMessageBox::critical(nullptr, QString("BPMMonitor"),
				      QString(ex.what()));
		QCoreApplication::exit(-1);
		assert(0 && "should have terminated");
	}
}

void TestControl::do_a_snapshot(void)
{
	// copy
	auto vec = data_;

	// get filename
	QString filename =
	    QFileDialog::getSaveFileName(this, "Where to save...");

	// open file
	QFile file(filename);
	if (!file.open(QIODevice::WriteOnly)) {
		QMessageBox::critical(nullptr, QString("File error"),
				      "Something went wrong");
		QCoreApplication::exit(-1);
		assert(0 && "should have terminated");
	}

	QTextStream stream(&file);

	uint16_t counter = 0;
	for (auto &d : data_) {
		stream << QString::number(counter) << "\t" << QString::number(d)
		       << "\n";
		++counter;
	}

	file.close();
}

void TestControl::send_command_to_bpm()
{
	using namespace bpmmonitor;
	auto current = comboBox->currentText();
	try {
		if (current == "FRAME") {
			auto vector = bpm_.frame();
			int counter(0);
			data_lines_.clear();
			for (const auto &i : vector) {
				QString qs("Got Value " + QString::number(i));
				textBrowser->append(qs);
				data_lines_.append(counter, i);
				++counter;
			}

		} else if (current == "START") {
			bpm_.start();
		} else if (current == "STOP") {
			bpm_.stop();
		} else if (current == "RUNNING") {
			textBrowser->append((bpm_.running()) ? "is running"
							     : "not runnning");
		}
	} catch (const BPMMonitorException &ex) {
		QMessageBox::critical(nullptr, QString("BPMMonitor"),
				      QString(ex.what()));
	}
}

void TestControl::get_new_data()
{
	data_ = bpm_.frame();
	data_lines_.clear();
	int counter = 0;
	for (auto &i : data_) {
		data_lines_.append(counter, i);
		// TODO: optional: write to file
		++counter;
	}
	// textBrowser->append("values updated");
}
