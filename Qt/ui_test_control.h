/********************************************************************************
** Form generated from reading UI file 'test_control.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TEST_CONTROL_H
#define UI_TEST_CONTROL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *mainwindow;
    QHBoxLayout *horizontalLayout;
    QFrame *frame_2;
    QVBoxLayout *verticalLayout;
    QComboBox *comboBox;
    QPushButton *pushButton;
    QFrame *line;
    QSpacerItem *verticalSpacer;
    QPushButton *snapshot;
    QFrame *line_2;
    QCheckBox *auto_updater;
    QTabWidget *tabWidget;
    QWidget *plain_return;
    QVBoxLayout *verticalLayout_3;
    QTextBrowser *textBrowser;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(800, 548);
        mainwindow = new QWidget(MainWindow);
        mainwindow->setObjectName(QStringLiteral("mainwindow"));
        horizontalLayout = new QHBoxLayout(mainwindow);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        frame_2 = new QFrame(mainwindow);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        verticalLayout = new QVBoxLayout(frame_2);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        comboBox = new QComboBox(frame_2);
        comboBox->setObjectName(QStringLiteral("comboBox"));

        verticalLayout->addWidget(comboBox);

        pushButton = new QPushButton(frame_2);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        verticalLayout->addWidget(pushButton);

        line = new QFrame(frame_2);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        snapshot = new QPushButton(frame_2);
        snapshot->setObjectName(QStringLiteral("snapshot"));
        snapshot->setEnabled(true);

        verticalLayout->addWidget(snapshot);

        line_2 = new QFrame(frame_2);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line_2);

        auto_updater = new QCheckBox(frame_2);
        auto_updater->setObjectName(QStringLiteral("auto_updater"));

        verticalLayout->addWidget(auto_updater);


        horizontalLayout->addWidget(frame_2);

        tabWidget = new QTabWidget(mainwindow);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        plain_return = new QWidget();
        plain_return->setObjectName(QStringLiteral("plain_return"));
        verticalLayout_3 = new QVBoxLayout(plain_return);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        textBrowser = new QTextBrowser(plain_return);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));

        verticalLayout_3->addWidget(textBrowser);

        tabWidget->addTab(plain_return, QString());

        horizontalLayout->addWidget(tabWidget);

        MainWindow->setCentralWidget(mainwindow);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "FRAME", Q_NULLPTR)
         << QApplication::translate("MainWindow", "START", Q_NULLPTR)
         << QApplication::translate("MainWindow", "STOP", Q_NULLPTR)
         << QApplication::translate("MainWindow", "RUNNING", Q_NULLPTR)
        );
#ifndef QT_NO_WHATSTHIS
        pushButton->setWhatsThis(QApplication::translate("MainWindow", "<html><head/><body><p>send the specified command</p></body></html>", Q_NULLPTR));
#endif // QT_NO_WHATSTHIS
        pushButton->setText(QApplication::translate("MainWindow", "send", Q_NULLPTR));
        snapshot->setText(QApplication::translate("MainWindow", "snapshot", Q_NULLPTR));
        auto_updater->setText(QApplication::translate("MainWindow", "EnableAutoUpdater", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(plain_return), QApplication::translate("MainWindow", "PlainReturn", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TEST_CONTROL_H
