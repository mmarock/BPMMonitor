

#ifndef BPMMANAGER_HH_INCLUDED
#define BPMMANAGER_HH_INCLUDED

#include <QObject>
#include <QString>

#include "../../src/BPMMonitor.hh"

namespace bpmmonitor
{

class BPMManager : public QObject
{

	Q_OBJECT

	BPMMonitor bpm_;

public:
	BPMManager(QString str);
	~BPMManager();

public slots:
	void process();

signals:
	void finished();
};

} // namespace bpmmonitor

#endif // BPMMANAGER_HH_INCLUDED
