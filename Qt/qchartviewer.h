

#ifndef QCHARTVIEWER_H_INCLUDED
#define QCHARTVIEWER_H_INCLUDED

#include <QWidget>

#include <QChart>

class QChartViewer : public QWidget {

	Q_OBJECT
	
public:
	QChartViewer(QWidget *parent = 0) : QWidget(parent) {
	}

	~QChartViewer() {}
};


#endif // QCHARTVIEWER_H_INCLUDED
