

#ifndef TEST_CONTROL_HH_INCLUDED
#define TEST_CONTROL_HH_INCLUDED

#include "../Client/BPMMonitor.hh"
#include "ui_test_control.h"

#include <QThread>
#include <QTimer>

#include <QtCharts/QChart>
#include <QtCharts/QLineSeries>

//#include <QChart>

class TestControl : public QMainWindow, Ui::MainWindow
{
	Q_OBJECT

	bpmmonitor::BPMMonitor bpm_;
	// QChart chart_;

	QThread timer_thr_;
	QTimer timer_;

	QtCharts::QChart chart_;

	std::vector<uint16_t> data_;
	QtCharts::QLineSeries data_lines_;

public:
	TestControl(QMainWindow *parent = 0);
	~TestControl();

	void setupBPMInterface(
	    void); // connect the widgets signals to the TestControl slots
	void setupChart(void);

private slots:
	void get_new_data(void);
	void send_command_to_bpm(void);

private:
	static bpmmonitor::BPMMonitor make_bpm_monitor(void);

	void do_a_snapshot(void);
};

#endif // TEST_CONTROL_HH_INCLUDED
