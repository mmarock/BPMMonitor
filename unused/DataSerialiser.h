

#ifndef DATASERIALISER_H_INCLUDED
#define DATASERIALISER_H_INCLUDED

// max is 10 bit-> 4 in hexa + 1 control byte (\n or \x04)
#define DATASERIALISER_BUFFER_SIZE 300l
#define DATASERIALISER_BUFFER_SENTINEL DATASERIALISER_BUFFER_SIZE - 5

#include <stdbool.h>
#include <stdint.h>

/**
 * @brief DataSerialiser Buffer. This takes
 * 	uint16_t values, serialises them to regular
 * 	ascii data and stores ist, until the struct
 * 	is initialised again.
 *
 * @note The stream is not terminated by a \0 byte.
 */
struct DataSerialiser {
	uint8_t *wptr;
	uint8_t buf[DATASERIALISER_BUFFER_SIZE];
};

void DataSerialiserInit(struct DataSerialiser *const ds);

/**
 * @brief Push an int into the buffer
 *
 * @param ds The serialiser
 *
 * @param value The integer you want to push
 *
 * Serialise the value and push it onto buf.
 * Size check is done before serialisation.
 * If there is no space anymore, this function returns NULL.
 *
 * @return NULL if buffer full, else value
 */
const uint16_t *const DataSerialiserPushInt(struct DataSerialiser *const ds,
					    const uint16_t *const value);

/**
 * @brief @see DataSerialiserPushInt()
 *
 * This function is used to inject control characters
 * into the data stream
 *
 * @param ds
 * @param ch
 *
 * @return NULL if full, else ptr to the written char
 */
const uint8_t *const DataSerialiserPushChar(struct DataSerialiser *const ds,
					    const uint8_t ch);

/**
 * @brief Get the buffer begin
 *
 * Convenience function for better looks. The pointer
 * points to der buffer begin constantly
 *
 * @param ds
 *
 * @return &ds.buf
 */
const uint8_t *DataSerialiserBegin(const struct DataSerialiser *const ds);

/**
 * @brief Get the buffer end
 *
 * Convenience function for better looks. The pointer
 * points past the end.
 *
 * @param ds
 *
 * @return ds->wptr + DATASERIALISER_BUFFER_SIZE
 */
const uint8_t *const DataSerialiserEnd(const struct DataSerialiser *const ds);

#ifdef DEBUG_ON_PC
/**
 * @brief Debug function for DataSerialiserTest
 *
 * @note To compile it, add -D DEBUG_ON_PC compiler flag
 *
 * @param ds
 */
void DataSerialiserPrint(const struct DataSerialiser *const ds);
#endif // DEBUG_ON_PC

/**
 * @brief Check if enough memory remains for another value
 *
 * @note "full" does not mean that the buffer is full to the brim.
 * 	This could not work because you can not say how much memory
 * 	is occupied by a number without converting it. Instead a define
 * 	@see DATASERIALISER_BUFFER_SENTINEL gives a memory barrier. If
 * 	wptr exceeded this memory, DataSerialiserFull() returns false.
 * 	If you want it on point, just push into the buffer without
 * 	checking (but the a conversion is done at every call).
 *
 * @param ds
 *
 * @return true if full (@see DATASERIALISER_BUFFER_SENTINEL), else false
 */
bool DataSerialiserFull(const struct DataSerialiser *const ds);

#endif // DATASERIALISER_H_INCLUDED
