

#include "Command.h"

#ifdef DEBUG_BUILD
#ifndef PC_DEBUG
#include <avr/pgmspace.h>
#else // PC_DEBUG
#define printf_P(x) printf(x)
#define PSTR(x) x
#endif // PC_DEBUG
#include <stdio.h>
#else // DEBUG_BUILD
#define NULL (void *)0
#endif // DEBUG_BUILD

#include "Message.h"

enum { START_POS = 0,
       CMD_POS = 1,
       RESULT_POS_HB = 2,
       RESULT_POS_LB = 3,
       END_POS = 4 };

void CommandInit(struct Command *const cmds)
{
	cmds->connection = NULL;
	cmds->type = CMD_LAST;
}

int8_t CommandParse(struct ConnectionHandler *const ch, const uint8_t *payload,
		    uint8_t payload_len, struct Command *cmd_buf,
		    uint8_t cmd_buf_len)
{
	const uint8_t *const payload_end = payload + payload_len;
	const struct Command *const cmd_buf_end = cmd_buf + cmd_buf_len;
	while (payload + sizeof(struct Message) <= payload_end) {
		// 0	1	2	3	4	5
		// stx	data	result	result	ste	new_stx

		// is stx byte given
		if (payload[START_POS] != MSG_CTRL_START) {
#ifdef DEBUG_BUILD
			printf_P(PSTR("CommandParse: first != stx\r\n"));
#endif // DEBUG_BUILD
			++payload;
			continue;
		}

		// is ste byte given
		if (payload[END_POS] != MSG_CTRL_STOP) {
#ifdef DEBUG_BUILD
			printf_P(PSTR("CommandParse: last != ste\r\n"));
#endif // DEBUG_BUILD
			++payload;
			continue;
		}

		// is buffer full
		if (cmd_buf >= cmd_buf_end) {
#ifdef DEBUG_BUILD
			printf_P(PSTR("CommandParse: cmd_buf overflow\r\n"));
#endif // DEBUG_BUILD
			return -1;
		}

		// is data valid
		uint8_t cmd = payload[CMD_POS];
		if (cmd >= CMD_LAST) {
#ifdef DEBUG_BUILD
			printf_P(PSTR("CommandParse: unknown cmd %d\r\n"), cmd);
#endif // DEBUG_BUILD
			return -1;
		}

		if (payload[RESULT_POS_HB] || *(payload + RESULT_POS_LB)) {
#ifdef DEBUG_BUILD
			printf_P(PSTR("CommandParse: have return value\r\n"));
#endif // DEBUG_BUILD
		}

		cmd_buf->connection = ch;
		cmd_buf->type = (enum COMMAND_TYPE)cmd;
		++cmd_buf;

		payload += sizeof(struct Message);
	};
	return cmd_buf_len - (cmd_buf_end - cmd_buf);
}
