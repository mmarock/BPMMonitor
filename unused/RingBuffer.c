

#include "RingBuffer.h"

#ifdef DEBUG_ON_PC
#include <assert.h>
#include <stdio.h>
#else

#define NULL ((void *)0)
#endif // DEBUG_ON_PC

void RingBufferInit(struct RingBuffer *const rb)
{
	rb->wptr = rb->data;
	rb->rptr = NULL;
}

const uint16_t *const RingBufferPush(struct RingBuffer *const rb,
				     const uint16_t *value)
{
	uint16_t *w = (uint16_t *)rb->wptr;
	const uint16_t *r = rb->rptr;

	// set r pointer
	if (!r) {
		rb->rptr = w;
	} else if (w == r) {
		return NULL;
	}

	// jump
	if (w + 1 >= (rb->data + RINGBUFFER_SIZE)) {
		w = rb->data;
	} else {
		w += 1;
	}

	// get value, refresh non local w
	*rb->wptr = *value;
	rb->wptr = w;
	return value;
}

const uint16_t *const RingBufferPop(struct RingBuffer *const rb,
				    uint16_t *const value)
{
	// get r ptr once
	uint16_t *r = rb->rptr;

	// no memory left
	if (!r) {
		return NULL;
	}

	// jump
	if (r + 1 >= (rb->data + RINGBUFFER_SIZE)) {
		r = rb->data;
	} else {
		r += 1;
	}

	// write mem and increment non local pointer
	*value = *(rb->rptr);
	rb->rptr = (r == (uint16_t *)rb->wptr) ? NULL : r;
	return value;
}

uint16_t RingBufferSize(const struct RingBuffer *const rb)
{
	if (!rb->rptr) {
		return 0;
	}
	int16_t c = rb->wptr - rb->rptr;
	return (c > 0) ? c : RINGBUFFER_SIZE + c;
}

#ifdef DEBUG_ON_PC
void RingBufferPrint(const struct RingBuffer *const rb)
{

	for (int i = 0; i <= RINGBUFFER_SIZE; ++i) {
		putchar((i == RINGBUFFER_SIZE) ? '#' : '-');
	}
	putchar('\n');
	for (int i = 0; i < RINGBUFFER_SIZE; ++i) {
		if ((rb->data + i == rb->wptr) && (rb->wptr == rb->rptr)) {
			putchar('x');
			continue;
		}

		if (rb->data + i == rb->wptr) {
			putchar('w');
		} else if (rb->data + i == rb->rptr) {
			putchar('r');
		} else {
			putchar(' ');
		}
	}
	putchar('\n');
	printf("data:%p\tw:%ld\tr:%ld\tmax:%ld\n", rb->data,
	       rb->wptr - rb->data, rb->rptr - rb->data,
	       (rb->data + RINGBUFFER_SIZE) - rb->data);
	// getchar();
}
#endif // DEBUG_ON_PC
