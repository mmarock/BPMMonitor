
#ifndef MESSAGE_H_INCLUDED
#define MESSAGE_H_INCLUDED

#include <stdint.h>

enum MESSAGE_CTRL_BYTES { MSG_CTRL_START = '\x02', MSG_CTRL_STOP = '\x03' };

struct Message {
	const uint8_t start; // 0
	uint8_t query;      // 1
	uint16_t result;     // 2 + 3
	const uint8_t stop;  // 4
};

/**
 * @brief Use this function to compose a message
 *
 * This function sets start and stop values right
 *
 * @return the message
 */
struct Message MessageCompose(const uint16_t *query,
			      const uint16_t *return_val);

#endif // MESSAGE_H_INCLUDED
