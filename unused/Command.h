

#ifndef COMMAND_H_INCLUDED
#define COMMAND_H_INCLUDED

#include <stdint.h>

enum COMMAND_TYPE {
	CMD_MOTOR_START = 'A',
	CMD_MOTOR_STOP,
	CMD_MEASURMENT_START,
	CMD_MEASURMENT_STOP,
	CMD_UNSET_GAIN,
	CMD_SET_GAIN1,
	CMD_SET_GAIN2,
	CMD_SET_GAIN3,
	CMD_SET_GAIN4,
	CMD_SET_GAIN5,
	CMD_SET_GAIN6,
	CMD_SET_GAIN7,
	CMD_GET_INFO,
	CMD_DISCONNECT,
	CMD_GET_FRAME,
	CMD_HELLO_WORLD,
	CMD_LAST
};

struct Command {
	struct ConnectionHandler *connection;
	enum COMMAND_TYPE type;
};

/**
 * @brief Initialise a command
 *
 * @param cmds
 */
void CommandInit(struct Command *const cmds);

/**
 * @brief Parse a payload of bytes to commands
 *
 * @param ch the associated Connection
 * @param payload Byte payload array
 * @param payload_len payload len
 * @param cmd_buf the Command buffer with the result
 * @param cmd_buf_len len of cmd_buf
 *
 * @return number of parsed commands or -1
 */
int8_t CommandParse(struct ConnectionHandler *const ch, const uint8_t *payload,
		    uint8_t payload_len, struct Command *cmd_buf,
		    uint8_t cmd_buf_len);

#endif // COMMAND_H_INCLUDED
