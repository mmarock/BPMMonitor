

#include "DataSerialiser.h"

#include <string.h>

#ifdef DEBUG_ON_PC
#include <stdio.h>
#include <stdlib.h>
#endif // DEBUG_ON_PC

void DataSerialiserInit(struct DataSerialiser *const ds) { ds->wptr = ds->buf; }

static uint8_t in_ascii(const uint8_t *const hexabyte)
{
	switch (*hexabyte) {
	case 0:
		return '0';
	case 1:
		return '1';
	case 2:
		return '2';
	case 3:
		return '3';
	case 4:
		return '4';
	case 5:
		return '5';
	case 6:
		return '6';
	case 7:
		return '7';
	case 8:
		return '8';
	case 9:
		return '9';
	case 10:
		return 'A';
	case 11:
		return 'B';
	case 12:
		return 'C';
	case 13:
		return 'D';
	case 14:
		return 'E';
	case 15:
		return 'F';
	default: // never happens, error case
		return 'G';
	};
}

static uint8_t unsigned_to_ascii(const uint16_t no, uint8_t *str, uint8_t len,
				 const uint8_t delim)
{
	uint8_t written = 0;
	for (int8_t i = 3; i >= 0; --i) {
		uint8_t hb = (no >> i * 4) & (0xF);
		hb = in_ascii(&hb);
		if (hb == '0' && !written) {
			continue;
		}
		*(str++) = hb;
		++written;
	}

	*(str++) = delim;
	++written;
	return written;
}

const uint16_t *const DataSerialiserPushInt(struct DataSerialiser *const ds,
					    const uint16_t *const value)
{
	uint8_t buf[8];

	// hex number: FFFF\t\0
	// 		------ - -
	// utoa(*value, (char *)buf, 16);
	uint8_t len = unsigned_to_ascii(*value, buf, sizeof(buf), '\t');

	if (len >= (DATASERIALISER_BUFFER_SIZE - (ds->wptr - ds->buf))) {
		return NULL;
	}

	// cpy without \0
	memcpy(ds->wptr, buf, len);
	ds->wptr += len;
	return value;
}

const uint8_t *const DataSerialiserPushChar(struct DataSerialiser *const ds,
					    const uint8_t ch)
{
	if (ds->wptr < ds->buf + DATASERIALISER_BUFFER_SIZE - 1) {
		*(ds->wptr) = ch;
		return ds->wptr++;
	}
	return NULL;
}

const uint8_t *DataSerialiserBegin(const struct DataSerialiser *const ds)
{
	return ds->buf;
}

const uint8_t *const DataSerialiserEnd(const struct DataSerialiser *const ds)
{
	return ds->wptr;
}

#ifdef DEBUG_ON_PC
void DataSerialiserPrint(const struct DataSerialiser *const ds)
{
	for (int i = 0; i < DATASERIALISER_BUFFER_SIZE; ++i) {
		putchar('-');
	}
	putchar('\n');
	for (int i = 0; i < DATASERIALISER_BUFFER_SIZE; ++i) {
		if (&ds->buf[i] == ds->wptr) {
			break;
		}

		if (ds->buf[i] == '\t') {
			putchar(' ');
		} else {
			putchar(ds->buf[i]);
		}
	}
	putchar('\n');
	for (int i = 0; i < DATASERIALISER_BUFFER_SIZE; ++i) {
		if (&ds->buf[i] == ds->wptr) {
			putchar('w');
		} else {
			putchar(' ');
		}
	}
	putchar('\n');
}
#endif // DEBUG_ON_PC

bool DataSerialiserFull(const struct DataSerialiser *const ds)
{
	return (((ds->wptr - ds->buf)) <= DATASERIALISER_BUFFER_SENTINEL);
}
