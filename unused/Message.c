

#include "Message.h"

struct Message MessageCompose(const uint16_t *query, const uint16_t *res)
{
	static struct Message msg = {.start = MSG_CTRL_START,
				     .stop = MSG_CTRL_STOP };
	msg.query = *query;
	msg.result = *res;
	// TODO muss eine Kopie wirklich sein?
	return msg;
}
