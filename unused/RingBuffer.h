

#ifndef RINGBUFFER_H_INCLUDED
#define RINGBUFFER_H_INCLUDED

#define RINGBUFFER_SIZE 50l

#include <stdint.h>

/**
 * @brief Simple Ring Buffer which stores
 * 	read and write position for fast access
 */
struct RingBuffer {
	volatile uint16_t *wptr;
	uint16_t *rptr;
	uint16_t data[RINGBUFFER_SIZE];
};

/**
 * @brief Initialise the Ring Buffer
 *
 *	The read position is unset at the beginning to flag
 *	that there are no values to read. If read equals write
 *	pointer, there is plenty to read but no memory left for
 *	write operation. The corresponding pointer (pop -> read and
 *	push -> write) is set only once in the functions to make
 *	modifying from ISR easy.
 *
 * @param rb The RingBuffer Handler
 */
void RingBufferInit(struct RingBuffer *const rb);

/**
 * @brief Push a value into the buffer
 *
 * @param rb The RingBuffer Handler
 * @param value an unsigned value of 16 bit
 *
 * @return NULL if full, else value
 */
const uint16_t *const RingBufferPush(struct RingBuffer *const rb,
				     const uint16_t *value);
/**
 * @brief Pop a value from the buffer
 *
 * @param rb The RingBuffer Handler
 * @param value pointer to a valid uint16_t buffer
 *
 * @return NULL if empty, else value
 */
const uint16_t *const RingBufferPop(struct RingBuffer *const rb,
				    uint16_t *const value);

/**
 * @brief number of values you can read
 *
 * @param rb The RingBuffer Handler
 *
 * @return number of readable values
 */
uint16_t RingBufferSize(const struct RingBuffer *const rb);

#ifdef DEBUG_ON_PC
/**
 * @brief Graphical buffer representation
 *
 * @note If you compile the test, add -D DEBUG_ON_PC precompiler flag
 *
 * @param rb The RingBuffer Handler
 */
void RingBufferPrint(const struct RingBuffer *const rb);
#endif // DEBUG_ON_PC

#endif // RINGBUFFER_H_INCLUDED
