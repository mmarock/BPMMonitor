

/* Copyright (C)
 * 2016 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#ifndef BOARD_H_INCLUDED
#define BOARD_H_INCLUDED

#include <avr/io.h>

#define SS PORTB6
#define MISO PORTB3
#define MOSI PORTB2
#define SCK PORTB1

#define SD_CS PORTD4
#define HWB PORTE2

#define IO8 PORTB4
#define IO9 PORTB5
#define IO10 PORTB6
#define IO11 PORTB7

#define IO12 PORTD6
#define IO13 PORTC7

#define RXLED PORTB0
#define TXLED PORTD5
#define LEONARDO_LED PORTC7

#define TX PORTD3
#define RX PORTD2

#define SDA PORTD1
#define SCL PORTD0

#define D0 PORTD2
#define D1 PORTD3
#define D2 PORTD1
#define D3 PORTD0
#define D4 PORTD4
#define D5 PORTC6
#define D6 PORTD7
#define D7 PORTE6

#define A0 PORTF7
#define A1 PORTF6
#define A2 PORTF5
#define A3 PORTF4
#define A4 PORTF1
#define A5 PORTF0

#define set(REG, PIN) REG |= (1 << PIN)

#define clr(REG, PIN) REG &= ~(1 << PIN)

#define toggle(REG, PIN) REG ^= (1 << PIN)

#endif // BOARD_H_INCLUDED
