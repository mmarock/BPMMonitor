

/* Copyright (C)
 * 2016 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#include "Parser.h"

////////////////////////////////////////////////////////////
// private functions
////////////////////////////////////////////////////////////

static const uint8_t *get_keywords(enum PARSER_ELEMENT pe)
{
	switch (pe) {
	case PE_ERR:
	case PE_LAST:
		break;
	case PE_MOTOR_START:
	case PE_MOTOR_STOP:
	case PE_MOTOR_STATE:
		return (const uint8_t *)"MOTOR";
	case PE_CLOSE:
		return (const uint8_t *)"CLOSE";
	case PE_FRAME:
		return (const uint8_t *)"FRAME";
	};
	return (const uint8_t *)"ERR";
}

static uint8_t get_keylen(enum PARSER_ELEMENT pe)
{
	switch (pe) {
	case PE_ERR:
	case PE_LAST:
		break;
	case PE_MOTOR_START:
	case PE_MOTOR_STOP:
	case PE_MOTOR_STATE:
		return 5;
	case PE_CLOSE:
	case PE_FRAME:
		return 5;
	};
	return 0;
}

static int is_element(const uint8_t *buf, enum PARSER_ELEMENT el)
{
	const uint8_t *const end = buf + get_keylen(el);
	const uint8_t *els = get_keywords(el);
	while (buf < end) {
		if (*(buf++) != *(els++)) {
			return 0;
		}
	}
	return 1;
}

static enum PARSER_ELEMENT parse_close(const uint8_t *buf, uint8_t buf_len)
{
	(void)buf;
	(void)buf_len;
	return PE_CLOSE;
}

static enum PARSER_ELEMENT parse_frame(const uint8_t *buf, uint8_t buf_len)
{
	(void)buf;
	(void)buf_len;
	return PE_FRAME;
}

static enum PARSER_ELEMENT parse_motor(const uint8_t *buf, uint8_t buf_len)
{
	const uint8_t *const end = buf + buf_len;
	while (buf < end) {
		switch (*buf) {
		case '1':
			return PE_MOTOR_START;
		case '0':
			return PE_MOTOR_STOP;
		case '?':
			return PE_MOTOR_STATE;
		default:
			break;
		};
		++buf;
	}
	return PE_ERR;
}

static const uint8_t *get_newline_pos(const uint8_t *buf, const uint8_t buf_len)
{
	const uint8_t *const end = buf + buf_len;
	while (buf < end && *buf != '\n') {
		++buf;
	}
	return (buf < end) ? buf : ((const uint8_t *)0);
}

////////////////////////////////////////////////////////////
// public
////////////////////////////////////////////////////////////

const uint8_t *ParserParse(const uint8_t *buf, const uint8_t buf_len,
			   enum PARSER_ELEMENT *pes, uint8_t pes_len,
			   uint8_t *const pes_final_len)
{
	// solange buf < buf_end
	const uint8_t *const buf_end = buf + buf_len;
	const enum PARSER_ELEMENT *const pes_end = pes + pes_len;

	while (buf < buf_end) {

		// get newline pos
		const uint8_t *newline = get_newline_pos(buf, buf_end - buf);
		if (!newline) {
			break;
		}
		while (buf < newline && pes < pes_end - 1) {
			switch (*buf) {
			case 'C':
				if (is_element(buf, PE_CLOSE)) {
					buf += get_keylen(PE_CLOSE);
					*(pes++) =
					    parse_close(buf, newline - buf);
					buf = newline;
				}
				break;
			case 'F':
				if (is_element(buf, PE_FRAME)) {
					buf += get_keylen(PE_FRAME);
					*(pes++) =
					    parse_frame(buf, newline - buf);
					buf = newline;
				}
				break;

			case 'M':
				if (is_element(buf, PE_MOTOR_START)) {
					buf += get_keylen(PE_MOTOR_START);
					*(pes++) =
					    parse_motor(buf, newline - buf);
					buf = newline;
				}
				break;

			default:
				break;
			};
			++buf;
		}
		++buf;
	}

	if (pes < pes_end - 1) {
		*(pes++) = PE_LAST;
	}
	*pes_final_len = pes_len - (pes_end - pes) - 1;
	return buf;
}
