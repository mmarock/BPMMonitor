

/* Copyright (C)
 * 2016 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#include "Motor.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>

#ifdef DEBUG_BUILD
#include <avr/pgmspace.h>
#include <stdio.h>
#endif // DEBUG_BUILD

#include "Board.h"
#include "System.h"

#include "i2c_master.h"

#define SLAVE_ADDR (uint8_t)(1 << 2 | 1 << 5 | 1 << 7)

#define DELAY_IN_MS 2000
#define MOTOR_MAX_VAL 0xff

static struct {
	bool is_running;
	void (*trigger_cb)(void);
} motor_;

// Externer Interrupt Motor tacho
// TODO: Kann auch ein Timer sein, nehme für den
// Test entweder einen Taster oder einen Timer
// TODO entprelle den Taster
ISR(INT2_vect)
{
	if (motor_.trigger_cb) {
		motor_.trigger_cb();
		EIFR |= (1 << INTF2);
	}
}

////////////////////////////////////////
//	static functions
////////////////////////////////////////

enum TI_ADDRESS {
	TA_FIRST = 0x00,
	TA_RW_SpeedCtrl1 = 0x00,
	TA_RW_SpeedCtrl2 = 0x01,
	TA_RW_DevCtrl = 0x02,
	TA_RW_EECtrl = 0x03,
	TA_RO_Status = 0x10,
	TA_RO_MotorSpeed1 = 0x11,
	TA_RO_MotorSpeed2 = 0x12,
	TA_RO_MotorPeriod1 = 0x13,
	TA_RO_MotorPeriod2 = 0x14,
	TA_RO_MotorKt1 = 0x15,
	TA_RO_MotorKt2 = 0x16,
	TA_RO_IPDPosition = 0x19,
	TA_RO_SupplyVoltage = 0x1A,
	TA_RO_SpeedCmd = 0x1B,
	TA_RO_SpeedCmdBuffer = 0x1C,
	TA_RO_FaultCode = 0x1E,
	TA_EE_MotorParam1 = 0x20,
	TA_EE_MotorParam2 = 0x21,
	TA_EE_MotorParam3 = 0x22,
	TA_EE_SysOpt1 = 0x23,
	TA_EE_SysOpt2 = 0x24,
	TA_EE_SysOpt3 = 0x25,
	TA_EE_SysOpt4 = 0x26,
	TA_EE_SysOpt5 = 0x27,
	TA_EE_SysOpt6 = 0x28,
	TA_EE_SysOpt7 = 0x29,
	TA_EE_SysOpt8 = 0x2A,
	TA_EE_SysOpt9 = 0x2B,
	TA_LAST = 0x2C
};

static uint8_t twi_read(uint8_t addr, uint8_t *val)
{
	return i2c_readReg(SLAVE_ADDR, addr, val, 1);
}

static uint8_t twi_write(uint8_t addr, uint8_t val)
{
	return i2c_writeReg(SLAVE_ADDR, addr, &val, 1);
}

static int init_driver(void)
{
	i2c_init();

	// check RW registers
	// and write them if needed

	uint8_t value[12];
	for (uint8_t i = 0; i < 12; ++i) {
		if (twi_read(TA_EE_MotorParam1 + i, value + i)) {
			return -1;
		}
	}

	/*
	const uint8_t reference_values[12] = {
		0x4A, 0x4E, 0x2A, 0x00,
		0x98, 0xE4, 0x7A, 0xFC,
		0x69, 0xB7, 0xAD, 0x0C
	};
	*/

#ifdef DEBUG_BUILD
	for (const uint8_t *ptr = value; ptr < value + 12; ++ptr) {
		printf_P(PSTR("motor ee: %d:%x\n"), ptr - value, *ptr);
	}
#endif // DEBUG_BUILD

	// disable sleep
	if (twi_write(TA_RW_EECtrl, 1 << 7)) {
		return -1;
	}

	// set initial speed to 0
	if (twi_write(TA_RW_SpeedCtrl2, 1 << 7)) {
		return -1;
	}
	if (twi_write(TA_RW_SpeedCtrl1, 0)) {
		return -1;
	}
	return 0;
}

static int eval_state_and_fault(void)
{
	uint8_t status, fault;
	if (twi_read(TA_RO_Status, &status) ||
	    twi_read(TA_RO_FaultCode, &fault)) {
		SystemSetErrno(SME_MOTOR_I2C_ERROR);
		return -1;
	}

	if (status & 0xf<<4) {
		if (status & 1 << 4) {
			SystemSetErrno(SME_MOTOR_MTR_LOCK);
		} else if (status & 1 << 5) {
			SystemSetErrno(SME_MOTOR_OVERCURRENT);
		} else if (status & 1 << 6) {
			SystemSetErrno(SME_MOTOR_SLEEP_STANDBY);
		} else if (status & 1 << 7) {
			SystemSetErrno(SME_MOTOR_OVER_TEMP);
		}
		return -1;
	}

	if (fault & 0x3f) {
		if (fault & 1 << 0) {
			SystemSetErrno(SME_MOTOR_LOCK_CURRENT_LIMIT);
		} else if (fault & 1 << 1) {
			SystemSetErrno(SME_MOTOR_ABNORMAL_SPEED);
		} else if (fault & 1 << 2) {
			SystemSetErrno(SME_MOTOR_ABNORMAL_KT);
		} else if (fault & 1 << 3) {
			SystemSetErrno(SME_MOTOR_NO_MOTOR);
		} else if (fault & 1 << 4) {
			SystemSetErrno(SME_MOTOR_STUCK_OPEN_LOOP);
		} else if (fault & 1 << 5) {
			SystemSetErrno(SME_MOTOR_STUCK_CLOSED_LOOP);
		}
		return -1;
	}
	return 0;
}

////////////////////////////////////////
//	public functions
////////////////////////////////////////

int MotorInit(void (*f)(void))
{
	// config rx pin as input
	clr(DDRD, DDD2);

	// enable pullup resistor
	// set(PORTD, PORTD2);
	set(PORTD, PORTD2);

	// enable external int on rising edge
	clr(EIMSK, INT2);
	//EICRA = (1 << ISC21) | (1 << ISC20);
	EICRA = (1 << ISC21);
	EIFR |= (1 << INTF2);

	if (init_driver()) {
		SystemSetErrno(SME_MOTOR_I2C_ERROR);
		return -1;
	}
	motor_.trigger_cb = f;
	motor_.is_running = false;
	return 0;
}

int MotorStart(void)
{
#ifdef DEBUG_BUILD
	printf_P(PSTR("motor: start\n"));
#endif // DEBUG_BUILD

	if (twi_write(TA_RW_SpeedCtrl2, 1 << 7)) {
		SystemSetErrno(SME_MOTOR_I2C_ERROR);
		return -1;
	}
	if (twi_write(TA_RW_SpeedCtrl1, MOTOR_MAX_VAL)) {
		SystemSetErrno(SME_MOTOR_I2C_ERROR);
		return -1;
	}

	_delay_ms(DELAY_IN_MS);

	if (eval_state_and_fault()) {
		twi_write(TA_RW_SpeedCtrl2, 1 << 7);
		twi_write(TA_RW_SpeedCtrl1, 0);
		// error inside function
#ifdef DEBUG_BUILD
		printf_P(PSTR("eval_state: -1\r\n"));
#endif // DEBUG_BUILD
		return -1;
	}

#ifdef DEBUG_BUILD
	printf_P(PSTR("motor: freq:%dHz\n"), MotorFreqInHz());
#endif // DEBUG_BUILD

	set(EIMSK, INT2);

	/*
	 * speed only is 0xff if full voltage attached with i2c
	uint8_t speed;
	do {
		if (twi_read(TA_RO_SpeedCmd, &speed)) {
			SystemSetErrno(SME_MOTOR_I2C_ERROR);
			return -1;
		}
		printf_P(PSTR("speed: %d\r\n"), speed);
	} while (speed != 0xff);
	*/

	motor_.is_running = true;
	return 0;
}

bool MotorRunning(void) { return motor_.is_running; }

int MotorStop(void)
{
#ifdef DEBUG_BUILD
	printf_P(PSTR("motor: stop\n"));
#endif // DEBUG_BUILD
	clr(EIMSK, INT2);

	if (twi_write(TA_RW_SpeedCtrl2, 1 << 7)) {
		SystemSetErrno(SME_MOTOR_I2C_ERROR);
		return -1;
	}
	if (twi_write(TA_RW_SpeedCtrl1, 0)) {
		SystemSetErrno(SME_MOTOR_I2C_ERROR);
		return -1;
	}

	_delay_ms(DELAY_IN_MS);

	if (eval_state_and_fault()) {
		return -1;
	}

	motor_.is_running = false;
	return 0;
}

uint8_t MotorFreqInHz(void)
{
	uint8_t speed1, speed2;

	twi_read(TA_RO_MotorSpeed1, &speed1);
	twi_read(TA_RO_MotorSpeed2, &speed2);
	// hz is speed1 << 8 | speed2 / 10
	// so for mHz the factor is 100
	uint8_t res = (speed1 << 8 | speed2)/10.0;
#ifdef DEBUG_BUILD
	printf_P(PSTR("motor: freq in Hz: %d\n"), res);
#endif // DEBUG_BUILD
	return res;
}
