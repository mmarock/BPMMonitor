

/* Copyright (C)
 * 2016 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#include "DebugStream.h"

#include <util/setbaud.h>

#include <avr/io.h>
#include <stdio.h>

/**
 * @brief put a char into the usart
 *
 * @param c
 * @param stream
 *
 * @return 0
 */
static int uart_putchar(char c, FILE *stream)
{

	if (c == '\n') {
		uart_putchar('\r', stream);
	}

	loop_until_bit_is_set(UCSR1A, UDRE1);
	UDR1 = c;
	return 0;
}

FILE usart_stream = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_WRITE);

void DebugStreamInit(void)
{
	// set baud rate
	UBRR1H = UBRRH_VALUE;
	UBRR1L = UBRRL_VALUE;
	// enable tx only
	UCSR1B = (1 << TXEN1);
	// set frame format ( 8data, 2 stop )
	UCSR1C = (1 << USBS1) | (3 << UCSZ10);

	stdout = &usart_stream;
	stderr = &usart_stream;
}
