

/* Copyright (C)
 * 2016 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#include "Board.h"
#include "Connection.h"
#include "DataAcquisition.h"
#include "Motor.h"
#include "Parser.h"
#include "System.h"

#ifdef DEBUG_BUILD
#include "DebugStream.h"
#include <avr/pgmspace.h>
#include <stdio.h>
#else
#include <stdlib.h>
#endif // DEBUG_BUILD

#include <avr/interrupt.h>
#include <avr/sleep.h>

#define CMD_QUEUE_SIZE 10

static int exec_commands(struct Connection *const ch,
			 enum PARSER_ELEMENT *const cmds, int8_t cmds_n)
{

	for (int8_t c = 0; c < cmds_n; ++c) {
#ifdef DEBUG_BUILD
		printf_P(PSTR("exec_commands: got cmd %c\n"), cmds[c]);
#endif // DEBUG_BUILD
		switch (cmds[c]) {
		// start the motor, start data aquisition
		case PE_MOTOR_START:
			if (!MotorRunning() && MotorStart()) {
				ConnectionSendMessage(ch, MSGT_ERROR,
						      SystemGetErrno());
				return -1;
			}
			if (!DataAcquisitionRunning()) {
				DataAcquisitionStart();
			}
			break;
		case PE_MOTOR_STOP:
			// stop the motor, stop data aquisition
			if (MotorRunning() && MotorStop()) {
				ConnectionSendMessage(ch, MSGT_ERROR,
						      SystemGetErrno());
				return -1;
			}
			if (DataAcquisitionRunning()) {
				DataAcquisitionStop();
			}
			break;
		case PE_MOTOR_STATE:
			ConnectionSendMessage(ch, MSGT_OK, 1);
			ConnectionSendUInt(ch, (MotorRunning()) ? 1 : 0);
			continue;
		case PE_FRAME:
			if (DataAcquisitionRunning()) {
				ConnectionSendData(ch);
				continue;
			}
			break;
		case PE_CLOSE:
			ConnectionSendMessage(ch, MSGT_OK, 0);
			ConnectionReset(ch);
			return 0;
		case PE_LAST:
			return 0;
		default:
			ConnectionSendMessage(ch, MSGT_ERROR,
					      SME_PARSER_UNKNOWN);
			continue;
		};
		// In most cases, a simple OK is returned
		ConnectionSendMessage(ch, MSGT_OK, 0);
	}
	return 0;
}

static int8_t handle_connections(struct Connection *ch,
				 const struct Connection *const connections_end,
				 int8_t *connected)
{
	enum PARSER_ELEMENT cmds[CMD_QUEUE_SIZE];
	ConnectionUpdate(ch, connections_end);
	while (ch < connections_end) {
		int8_t cmds_n =
		    ConnectionProcess(ch, cmds, CMD_QUEUE_SIZE, connected);
		if (cmds_n > 0 && exec_commands(ch, cmds, cmds_n)) {
			// error management
		}
		++ch;
	}
	return 0;
}

int main(void)
{
#ifdef DEBUG_BUILD
	DebugStreamInit();
#endif // DEBUG_BUILD

	sei();

	// setup wizchip, dhcp
	if (SystemInit()) {
		// error management
		goto on_error;
	}
#ifdef DEBUG_BUILD
	printf_P(PSTR("main: System init\n"));
#endif // DEBUG_BUILD

	// sock 0: dhcp (handled in System)
	// sock 1: data
	// sock 2: data
	struct Connection connections[2];
	if (ConnectionInit(connections + 0, 1, 7500) ||
	    ConnectionInit(connections + 1, 2, 7500)) {
		goto on_error;
	}

	const struct Connection *const connections_end =
	    connections + sizeof(connections) / sizeof(struct Connection);

#ifdef DEBUG_BUILD
	printf_P(PSTR("main: Connection finished\n"));
#endif // DEBUG_BUILD

	if (MotorInit(DataAcquisitionEOS)) {
		goto on_error;
	}

	DataAcquisitionInit();

#ifdef DEBUG_BUILD
	printf_P(PSTR("main: enter loop\n"));
#endif // DEBUG_BUILD

	int8_t connected = 0;
	while (!SystemGetErrno()) {
		// handle dhcp
		SystemHandleDhcp(connections, connections_end);

		// handle connection
		handle_connections(connections, connections_end, &connected);
		if (connected <= 0) {
			if (DataAcquisitionRunning()) {
				DataAcquisitionStop();
			}
			if (MotorRunning()) {
				MotorStop();
			}
			sleep_mode();
		}
	}
on_error:
	// error management
	while (1) {
		// runs every second
		SystemVisualizeError();
		sleep_mode();
	}
}
