

/* Copyright (C)
 * 2016 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#include "Connection.h"

#include "Buffer.h"
#include "DataAcquisition.h"
#include "System.h"

#ifdef DEBUG_BUILD
#include <stdio.h>
#endif // DEBUG_BUILD
#include <avr/pgmspace.h>

#include "socket.h"
#include "W5500/w5500.h"

#define PREFERRED_MIN_VALUE_PAYLOAD 40

////////////////////////////////////////////////////////////////////////////////
//			static functions
////////////////////////////////////////////////////////////////////////////////

/**
 * @brief get the socket state
 *
 * @return the state or -1 and errno is set
 */
static int8_t get_socket_state(const struct Connection *const ch)
{
	uint8_t c = 0;
	switch (getsockopt(ch->socket, SO_STATUS, (void *)&c)) {
	case SOCK_OK:
		return (c > 0) ? c : -c;
	case SOCKERR_SOCKNUM:
		SystemSetErrno(SME_GETSOCKOPT_NUM);
		break;
	case SOCKERR_SOCKOPT:
		SystemSetErrno(SME_GETSOCKOPT_OPT);
		break;
	case SOCKERR_SOCKMODE:
		SystemSetErrno(SME_GETSOCKOPT_MODE);
		break;
	};
	return -1;
}

/**
 * @brief get the corresponding ascii char
 */
static uint8_t in_ascii(const uint8_t *const hexabyte)
{
	switch (*hexabyte) {
	case 0:
		return '0';
	case 1:
		return '1';
	case 2:
		return '2';
	case 3:
		return '3';
	case 4:
		return '4';
	case 5:
		return '5';
	case 6:
		return '6';
	case 7:
		return '7';
	case 8:
		return '8';
	case 9:
		return '9';
	case 10:
		return 'A';
	case 11:
		return 'B';
	case 12:
		return 'C';
	case 13:
		return 'D';
	case 14:
		return 'E';
	case 15:
		return 'F';
	default: // never happens, error case
		return '?';
	};
}

/**
 * @brief serialize the uint16_t value
 *
 * @param str buffer, must have enough memory!
 * @param delim the delimiter after the hex val
 *
 * @return number of written chars
 */
static uint8_t unsigned_to_ascii(const uint16_t *no, uint8_t *str,
				 const uint8_t delim)
{
	*(str++) = '0'; // 1 byte
	*(str++) = 'x'; // 1 byte
	uint8_t written = 2;

	for (int8_t i = 3; i >= 0; --i) {
		uint8_t hb = (*no >> i * 4) & (0xF);
		hb = in_ascii(&hb);
		if (hb == '0' && written < 2) {
			continue;
		}
		*(str++) = hb;
		++written;
	} // max 4 byte

	// if nothing written after 0x
	if (written <= 2) {
		*(str++) = '0'; // 1 byte
		++written;
	}

	*(str++) = delim; // 1 byte
	++written;

	// total: [4,7] bytes
	return written;
}

static int16_t min(const int16_t x, const int16_t y) { return (x < y) ? x : y; }

static uint8_t remaining_buf_size(const struct Connection *const ch)
{
	return sizeof(ch->rxbuffer) - (ch->rxbuffer_ptr - ch->rxbuffer);
}

static void clear_buffer(struct Connection *const ch)
{
	ch->rxbuffer_ptr = ch->rxbuffer;
}

/**
 * @brief send data
 *
 * @param ch the Connection
 * @param buf the given buffer
 * @param len the given buffers len
 *
 * @return written bytes
 */
static int16_t send_data(const struct Connection *const ch, const uint8_t *buf,
			 uint16_t len)
{
	// TODO look out for send() functions with signed integers
	int16_t c = 0;
	uint16_t written = 0;

	while (written < len && (c = send(ch->socket, (uint8_t *)buf + written,
					  len - written)) > 0) {
		written += c;
	}
	if (c < 0) {
		switch (c) {
		case SOCKERR_SOCKSTATUS:
			SystemSetErrno(SME_SEND_STATUS);
			break;
		case SOCKERR_TIMEOUT:
			SystemSetErrno(SME_SEND_TIMEOUT);
			break;
		case SOCKERR_SOCKMODE:
			SystemSetErrno(SME_SEND_MODE);
			break;
		case SOCKERR_SOCKNUM:
			SystemSetErrno(SME_SEND_NUM);
			break;
		case SOCKERR_DATALEN:
			SystemSetErrno(SME_SEND_DATALEN);
			break;
		case SOCK_BUSY:
			SystemSetErrno(SME_SEND_BUSY);
			break;
		};
		return c;
	}
	return written;
}

static int8_t connection_parse(struct Connection *const ch,
			       enum PARSER_ELEMENT *cmds, uint8_t cmds_n)
{

	// reset buffer, if full without a good parse
	if (ch->rxbuffer_ptr >= ch->rxbuffer + sizeof(ch->rxbuffer)) {
		clear_buffer(ch);
	}

	uint16_t rx = 0;
	getsockopt(ch->socket, SO_RECVBUF, (void *)&rx);
	int16_t len = min(rx, remaining_buf_size(ch));

	// have len, now receive
	int16_t c = 0;
	while (len > 0 && (c = recv(ch->socket, ch->rxbuffer_ptr, len)) > 0) {
		ch->rxbuffer_ptr += c;
		len -= c;
#ifdef DEBUG_BUILD
		printf_P(
		    PSTR("Chandler: recv: len:%d, c:%d\r\nrxbuffer_ptr:%p, "
			 "rxbuffer:%p\n"),
		    len, c, ch->rxbuffer_ptr, ch->rxbuffer);
#endif // DEBUG_BUILD
	}
	if (c < 0) {
		switch (c) {
		case SOCKERR_SOCKSTATUS:
			SystemSetErrno(SME_RECV_STATUS);
			break;
		case SOCKERR_SOCKMODE:
			SystemSetErrno(SME_RECV_MODE);
			break;
		case SOCKERR_SOCKNUM:
			SystemSetErrno(SME_RECV_NUM);
			break;
		case SOCKERR_DATALEN:
			SystemSetErrno(SME_RECV_DATALEN);
			break;
		case SOCK_BUSY:
			SystemSetErrno(SME_RECV_BUSY);
			break;
		};
		return c;
	}
	// recvd and error checked, now try parse
	uint8_t cmds_counted = 0;
	const uint8_t *parsed_end =
	    ParserParse(ch->rxbuffer, ch->rxbuffer_ptr - ch->rxbuffer, cmds,
			cmds_n, &cmds_counted);

	// jetzt: parsed_end bis buffer_ptr zum Anfang kopieren
	const uint8_t *const data_end = ch->rxbuffer_ptr;
	clear_buffer(ch);
	while (parsed_end < data_end) {
		*(ch->rxbuffer_ptr++) = *(parsed_end++);
	}
	return (int8_t)cmds_counted;
}

////////////////////////////////////////////////////////////////////////////////
//			public functions
////////////////////////////////////////////////////////////////////////////////

int8_t ConnectionInit(struct Connection *const ch, int8_t sock, uint16_t port)
{
	ch->socket = -1;
	ch->port = port;
	ch->interrupt = 0;
	// set the buffer to init position
	clear_buffer(ch);

	sockint_kind sint =
	    SIK_CONNECTED | SIK_DISCONNECTED | SIK_RECEIVED | SIK_TIMEOUT;
	ctlsocket(sock, CS_SET_INTMASK, &sint);

	switch (socket(sock, SOCK_STREAM, port, 0)) {
	case SOCKERR_SOCKNUM:
		SystemSetErrno(SME_SOCKET_NUM);
		return -1;
	case SOCKERR_SOCKMODE:
		SystemSetErrno(SME_SOCKET_MODE);
		return -1;
	case SOCKERR_SOCKFLAG:
		SystemSetErrno(SME_SOCKET_FLAG);
		return -1;
	case SOCK_OK:
		break;
	};

	switch (listen(sock)) {
	case SOCKERR_SOCKINIT:
		SystemSetErrno(SME_LISTEN_INIT);
		return -1;
	case SOCKERR_SOCKCLOSED:
		SystemSetErrno(SME_LISTEN_CLOSED);
		return -1;
	case SOCK_OK:
		break;
	};

	ch->socket = sock;
	return 0;
}

void ConnectionUpdate(struct Connection *it, const struct Connection *const end)
{
	// get interrupts
	intr_kind ints = wizchip_getinterrupt();
	if (!ints) {
		return;
	}
	while (it < end) {
		// IK_SOCK_0 is 1 << 8
		// then is sock 0 -> 1 << 8 << 0 == 1 << 8
		// then is sock 1 -> 1 << 8 << 1 == 1 << 9...
		uint16_t this_sock = (IK_SOCK_0 << it->socket) & ints;
		if (this_sock) {
			wizchip_clrinterrupt(this_sock);
			ctlsocket(it->socket, CS_GET_INTERRUPT, &it->interrupt);
		} else {
			it->interrupt = 0;
		}
		++it;
	}
}

int8_t ConnectionProcess(struct Connection *const ch,
			 enum PARSER_ELEMENT *elements, uint8_t cmds_n,
			 int8_t *connected)
{

	uint16_t tmp = 0;
	if (ch->interrupt & SIK_CONNECTED) {
		tmp = SIK_CONNECTED;
		ctlsocket(ch->socket, CS_CLR_INTERRUPT, &tmp);
		*connected += 1;
	} else if (ch->interrupt & SIK_DISCONNECTED) {
		tmp = SIK_DISCONNECTED;
		ctlsocket(ch->socket, CS_CLR_INTERRUPT, &tmp);
		*connected -= 1;
		ConnectionReset(ch);
		return 0;
	}
	if (ch->interrupt & SIK_RECEIVED) {
		tmp = SIK_RECEIVED;
		ctlsocket(ch->socket, CS_CLR_INTERRUPT, &tmp);
		return connection_parse(ch, elements, cmds_n);
	}
	if (ch->interrupt & SIK_TIMEOUT) {
		tmp = SIK_TIMEOUT;
		ctlsocket(ch->socket, CS_CLR_INTERRUPT, &tmp);
		*connected -= 1;
		ConnectionReset(ch);
	}
	if (ch->interrupt & SIK_SENT) {
		tmp = SIK_SENT;
		ctlsocket(ch->socket, CS_CLR_INTERRUPT, &tmp);
	}
	return 0;
}

int8_t ConnectionReset(struct Connection *const ch)
{
	switch (get_socket_state(ch)) {
	case SOCK_LISTEN:
		return 0;
	case -1:
		return -1;
	default:
		break;
	};

	if (close(ch->socket) != SOCK_OK) {
		SystemSetErrno(SME_CLOSE_NUM);
		return -1;
	}

	if (ConnectionInit(ch, ch->socket, ch->port)) {
		return -1;
	}
	return 0;
}

int16_t ConnectionSendData(const struct Connection *const ch)
{
#ifdef DEBUG_BUILD
	printf_P(PSTR("Chandler: try send\n"));
#endif // DEBUG_BUILD
	DataAcquisitionLockBuffer();
	const struct Buffer *const b = DataAcquisitionGetBuffer();
	const uint16_t *it = BufferBegin(b);
	const uint16_t *const mem_end = BufferMemoryEnd(b);
	uint8_t buf[256];

	ConnectionSendMessage(ch, MSGT_OK, BUFFER_SIZE);
	while (it < mem_end) {
		// decide if there are enough values produced
		const uint16_t *const end = BufferEnd(b);
		if (end - it < PREFERRED_MIN_VALUE_PAYLOAD &&
		    mem_end - it >= PREFERRED_MIN_VALUE_PAYLOAD) {
			continue;
		}
		// fill buffer
		// TODO remove magic number
		uint8_t *bufptr = buf;
		while (it < end && bufptr + 10 < buf + sizeof(buf)) {
			bufptr += unsigned_to_ascii(it, bufptr, '\n');
			++it;
		}

		if (send_data(ch, buf, bufptr - buf) < 0) {
			DataAcquisitionUnlockBuffer();
			return -1;
		}
	}
	DataAcquisitionUnlockBuffer();
	return (mem_end - it) / sizeof(uint16_t);
}

int16_t ConnectionSendUInt(const struct Connection *const ch, uint16_t value)
{
	// fill buffer
	uint8_t buf[16];
	return send_data(ch, buf, unsigned_to_ascii(&value, buf, '\n'));
}

void ConnectionSendMessage(const struct Connection *const ch, enum MSG_TYPE mt,
			   uint16_t value)
{

	uint8_t buf[16];
	uint8_t *bptr = buf;
	// "ERR\t255\r\n"

	// TODO
	// set msg_type: OK or ERROR
	{
		PGM_P mtptr = (mt == MSGT_OK) ? PSTR("OK\t") : PSTR("ERR\t");
		while (pgm_read_byte(mtptr) != '\0') {
			*(bptr++) = pgm_read_byte(mtptr);
			++mtptr;
		}
	}

	// set val
	bptr += unsigned_to_ascii(&value, bptr, '\r');

	// set \r\n
	*(bptr++) = '\n';

	send_data(ch, buf, bptr - buf);
}
