

/* Copyright (C)
 * 2016 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#ifndef PARSER_H_INCLUDED
#define PARSER_H_INCLUDED

#include <stdint.h>

/**
 * @brief enum representation of parsed command lines
 *
 * @file Parser.h
 *
 * This file contains a line parser which iterates over a received
 * byte buffer and extracts the commands as enum array
 *
 */

enum PARSER_ELEMENT {
	PE_ERR = -1,
	PE_MOTOR_START = 'A',
	PE_MOTOR_STOP,
	PE_MOTOR_STATE,
	PE_CLOSE,
	PE_FRAME,
	PE_LAST
};

/**
 * @brief Parse elements from an uint8_t buffer
 *
 * @param buf byte buffer
 * @param buf_len len of byte buffer
 * @param pes result array
 * @param pes_len len of result array
 * @param pes_final_len real len of PARSER_ELEMENT array
 *
 * @note pes array must be big enough to hold the result
 *
 * @return position in buffer, the Parser stopped parsing
 */
const uint8_t *ParserParse(const uint8_t *buf, const uint8_t buf_len,
			   enum PARSER_ELEMENT *pes, uint8_t pes_len,
			   uint8_t *const pes_final_len);

#endif // PARSER_H_INCLUDED
