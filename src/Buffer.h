

/* Copyright (C)
 * 2016 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#ifndef BUFFER_H_INCLUDED
#define BUFFER_H_INCLUDED

#define BUFFER_SIZE 300

#include <stdint.h>

struct Buffer {
	uint16_t *wptr;
	uint16_t data[BUFFER_SIZE];
};

/**
 * @brief set wptr to data
 */
void BufferInit(struct Buffer *const b);

/**
 * @brief get the actual used buffer size
 *
 * @return buffer size
 */
uint16_t BufferSize(const struct Buffer *const b);

/**
 * @brief push a value into the buffer
 *
 * @param val value you want to push
 *
 * @return ptr to pushed value or NULL, if full
 */
const uint16_t *BufferPush(struct Buffer *const b, const uint16_t *val);

/**
 * @brief get data begin
 *
 * @return data + 0
 */
const uint16_t *BufferBegin(const struct Buffer *b);

/**
 * @brief get past the end ptr
 *
 * @return wptr
 */
const uint16_t *BufferEnd(const struct Buffer *b);

const uint16_t *BufferMemoryEnd(const struct Buffer *const b);

/**
 * @brief same like BufferInit(), but looks better
 */
void BufferClear(struct Buffer *const b);

#endif // BUFFER_H_INCLUDED
