

/* Copyright (C)
 * 2016 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#include "System.h"
#include "config.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <util/delay.h>

#include <stdlib.h>

#include "Board.h"
#include "Connection.h"
#ifdef DEBUG_BUILD
#include <stdio.h>
#define _DHCP_DEBUG_
#endif // _DHCP_DEBUG_
#include "dhcp.h"
#include "wizchip_conf.h"

#define SM_DHCP_SOCKET 0

static struct {
	enum SYSTEM_ERRNO errno;
	uint8_t dhcp_buffer[548]; // look into dhcp.c -> RIP_MSG_SIZE
	wiz_NetInfo info;
} data;

////////////////////////////////////////////////////////////////////////////////
//		private functions
////////////////////////////////////////////////////////////////////////////////

/**
 * @brief cb function for spi bus
 */
static void spi_wb(uint8_t wb)
{
	SPDR = wb;
	loop_until_bit_is_set(SPSR, SPIF);
}

/**
 * @brief cb function for spi bus
 *
 * @return the received byte
 */
static uint8_t spi_rb(void)
{
	// already done in spi_wb
	// loop_until_bit_is_clear(SPSR, SPIF);
	spi_wb(0);
	return SPDR;
}

// spi burst r/w
// static void spi_brb(uint8_t *pBuf, uint16_t len) {}
// static void spi_bwb(uint8_t *pBuf, uint16_t len) {}

// TODO: Check if this is needed
static void cs_sel(void) { clr(PORTB, SS); }

static void cs_desel(void) { set(PORTB, SS); }

static void ip_assign(void)
{
	getIPfromDHCP(data.info.ip);
	getGWfromDHCP(data.info.gw);
	getSNfromDHCP(data.info.sn);
	getDNSfromDHCP(data.info.dns);
	data.info.dhcp = NETINFO_DHCP;
	wizchip_setnetinfo(&data.info);
#ifdef DEBUG_BUILD
	printf("ip-assign: %d-%d-%d-%d\r\n", data.info.ip[0], data.info.ip[1],
	       data.info.ip[2], data.info.ip[3]);
#endif // DEBUG_BUILD
}

static void ip_conflict(void) { SystemSetErrno(SME_DHCP_IP_CONFLICT); }

// 1s time handler
ISR(TIMER1_OVF_vect)
{
	TCNT1 = 49911;
	DHCP_time_handler();
}

////////////////////////////////////////////////////////////////////////////////
//		public functions
////////////////////////////////////////////////////////////////////////////////

int8_t SystemInit(void)
{
	// red led
	DDRD |= (1 << DDD4);
	clr(PORTD, PORTD4);

	// green led
	DDRE |= (1 << DDE6);
	clr(PORTE, PORTE6);

	data.errno = SME_NONE;
	// setup timer 1
	// OCIE1A Compare Interrupt Enable 1A
	// CS11 Clock Select clk/1024
	TIMSK1 = 1 << TOIE1;
	// preload
	TCNT1 = 49911;
	TCCR1B = (1 << CS10) | (1 << CS12);

	// Enable Spi Port (MOSI and SCK to 1)
	DDRB |= (1 << MOSI) | (1 << SCK) | (1 << SS) | (1 << DDB0) |
		(1 << DDB6); // | (1<<SS);
	set(PORTB, PORTB0);  // SS pin from the spi interface
	set(PORTB, PORTB6);  // SS pin from the spi interface

	// set(PORTB,PORTB0);
	// Enable Spi, Master, clock/16
	SPCR = (1 << SPE) | (1 << MSTR) | (1 << SPR0);
	// TODO: Enabling this did work in simple tests.
	// 	One step further and it failed, enable this on
	// 	the next real data acquisition and then decide
	// SPSR = 1 << SPI2X;

	reg_wizchip_spi_cbfunc(spi_rb, spi_wb);
	reg_wizchip_cs_cbfunc(cs_sel, cs_desel);
	if (wizchip_init(NULL, NULL)) {
		SystemSetErrno(SME_WIZCHIP_INIT);
		return -1;
	}

	wizphy_reset();
	while (!wizphy_getphypmode())
		;

	// TODO Prüfe, was alles nötig ist
	wiz_PhyConf phy = { .by = PHY_CONFBY_HW,
			    .mode = PHY_MODE_AUTONEGO,
			    .speed = PHY_SPEED_100,
			    .duplex = PHY_DUPLEX_FULL };
	wizphy_setphyconf(&phy);
	wizphy_setphypmode(PHY_POWER_NORM);

	data.info.mac[0] = W5500_MAC_0;
	data.info.mac[1] = W5500_MAC_1;
	data.info.mac[2] = W5500_MAC_2;
	data.info.mac[3] = W5500_MAC_3;
	data.info.mac[4] = W5500_MAC_4;
	data.info.mac[5] = W5500_MAC_5;
	data.info.dhcp = NETINFO_DHCP;
	wizchip_setnetinfo(&data.info);

	DHCP_init(SM_DHCP_SOCKET, data.dhcp_buffer);
	reg_dhcp_cbfunc(NULL, NULL, ip_conflict);

	if (wizchip_setnetmode(0)) {
		SystemSetErrno(SME_SYSTEM_SETNETMODE);
		return -1;
	}
	wizchip_setinterruptmask(IK_SOCK_ALL);
	return 0;
}

int8_t SystemHandleDhcp(struct Connection *it,
			const struct Connection *const end)
{
	// check physical connection
	if (!wizphy_getphylink()) {
		clr(PORTE, PORTE6);
		DHCP_stop();
		return 0;
	}

	switch (DHCP_run()) {
	case DHCP_FAILED:
		clr(PORTE, PORTE6);
		SystemSetErrno(SME_DHCP_FAILED);
		return -1;
	case DHCP_RUNNING:
	case DHCP_IP_LEASED:
		break;
	case DHCP_IP_CHANGED:
	case DHCP_IP_ASSIGN:
		set(PORTE, PORTE6);
		ip_assign();
		while (it < end) {
			ConnectionReset(it);
			++it;
		}
		break;
	case DHCP_STOPPED:
		DHCP_init(SM_DHCP_SOCKET, data.dhcp_buffer);
		break;
	}

	return 0;
}

enum SYSTEM_ERRNO SystemGetErrno(void) { return data.errno; }

void SystemSetErrno(enum SYSTEM_ERRNO err) { data.errno = err; }

void SystemVisualizeError(void)
{

	clr(PORTD, PORTD4);
	clr(PORTE, PORTE6);

#ifdef DEBUG_BUILD
	printf("System: err: %d\r\n", data.errno);
#endif // DEBUG_BUILD

	// 1s timer still runs
	sleep_mode();
	for (uint8_t i = 0; i < 7; ++i) {
		toggle(PORTE, PORTE6);
		if (data.errno & (1 << i)) {
			set(PORTD, PORTD4);
		} else {
			clr(PORTD, PORTD4);
		}
		sleep_mode();
	}
}
