

/* Copyright (C)
 * 2016 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#include "DataAcquisition.h"

#include "Buffer.h"
#include "System.h"

#include "Motor.h"

#ifdef DEBUG_BUILD
#include <avr/pgmspace.h>
#include <stdio.h>
#endif // DEBUG_BUILD

#include <avr/interrupt.h>
#include <avr/io.h>
#include <compat/ina90.h>

// must be in sync in the code!!
#define PRESCALER 64l

static struct {
	struct Buffer buf;
	volatile bool buf_lk;
	bool is_running;
} data_;

static const uint8_t get_cmpa_value(void)
{
	const double const_expr =
	    (const double)F_CPU / BUFFER_SIZE / PRESCALER;
	return (const uint8_t)(const_expr / MotorFreqInHz());
}

ISR(ADC_vect) // ADC_CONVERSION_COMPLETE
{
	// reset the cmp match interrupt flag
	TIFR0 |= (1 << OCF0A);
	// read ADCH last to make a new conversion possible
	uint16_t val = ADCL;
	val |= ADCH << 8;
	// if push failed -> buffer is full
	if (!BufferPush(&data_.buf, &val)) {
		// disable timer
		TCCR0B = 0;
	}
}

void DataAcquisitionInit(void)
{
	BufferInit(&(data_.buf));
	data_.buf_lk = false;
	data_.is_running = false;
	// TCCR0A = Timer/Counter Control Register A
	// WGM1 Waveform Generation Mode: Timer CTC Mode
	// -> timer runs from 0 to 0CR0
	TCCR0A = (1 << WGM01);

	// init the counter
	TCNT0 = 0;
	// set max to 33 = 300 samples, 25Hz Motor, prescaler = 64
	// TODO: set to define above if this works
	// OCR0 = 0x21;

	// REFS0 = AVcc with external capacitor on  AREF pin
	// ADMUX = ADC Multiplexer
	// MUX0, MUX1, MUX2 = ADC7 single ended input (A0 on Arduino Leonardo)
	// with MUX5 = Temp Sensor
	ADMUX = (1 << REFS0) | (1 << MUX0) | (1 << MUX1) | (1 << MUX2);
	// Choose Timer/Counter0 Compare Match A as Auto Trigger Source
	ADCSRB = (1 << ADTS1) | (1 << ADTS0);
	// Digital Input Disable for A0
	DIDR0 = (1 << ADC7D);

	// ADATE = ADC Auto Trigger Enable
	// ADIE = ADC Interrupt Enable
	// ADPS0 and ADPS2: prescaler set to clock/32
	ADCSRA = (1 << ADATE) | (1 << ADIE) | (1 << ADPS0) | (1 << ADPS2);
}

void DataAcquisitionStart(void)
{
	// ADEN = ADC Enable
	// ADSC = ADC Start Conversion
	ADCSRA |= (1 << ADEN) | (1 << ADSC);

	// Timer/Counter Interrupt Mask Register
	// OCIEA0A = Enable Compare Match A Interrupt
	// TIMSK0 |= (1 << OCIE0A);

	// set timer !before an overflow
	// BUT after the compare match
	// because we do not get this compare/match anyway.
	// It is most important to start from a defined value
	// to get the same data range
	uint8_t cmpa = get_cmpa_value();
	OCR0A = cmpa;
	TCNT0 = cmpa + 1;

#ifdef DEBUG_BUILD
	printf_P(PSTR("cmpa: %d"), cmpa);
#endif // DEBUG_BUILD

	// prescaler: CS01 and CS00: Set prescaler to clock/64
	TCCR0B = (1 << CS01) | (1 << CS00);
	data_.is_running = true;
}

void DataAcquisitionEOS(void)
{
	if (!data_.buf_lk) {
#ifdef DEBUG_BUILD
		printf("C\n");
#endif // DEBUG_BUILD
		BufferClear(&data_.buf);
		// reenable timer
		TCCR0B = (1 << CS01) | (1 << CS00);
	}
}

bool DataAcquisitionRunning(void) { return data_.is_running; }

void DataAcquisitionStop(void)
{
	// ADEN = ADC Enable
	ADCSRA &= ~(1 << ADEN);

	// disable timer
	TCCR0B = 0;

	// if 1 << ADSC is one, a conversion is in progress
	loop_until_bit_is_clear(ADCSRA, ADSC);

	BufferInit(&data_.buf);
	data_.is_running = false;
}

uint16_t DataAcquisitionSize(void) { return BufferSize(&data_.buf); }

bool DataAcquisitionEmpty(void)
{
	return !BufferSize(DataAcquisitionGetBuffer());
}

const struct Buffer *DataAcquisitionGetBuffer(void) { return &data_.buf; }

void DataAcquisitionLockBuffer(void) { data_.buf_lk = true; }

void DataAcquisitionUnlockBuffer(void) { data_.buf_lk = false; }
