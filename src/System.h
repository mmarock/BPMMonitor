

/* Copyright (C)
 * 2016 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#ifndef SYSTEM_H_INCLUDED
#define SYSTEM_H_INCLUDED

/**
 *
 * @file System.h
 *
 * @brief Set some crucial features for the Arduino Leonardo Board and my
 *software
 *
 * This module does some important basic things.
 * It implements an errno implementation, initialises
 * spi and the wiznet chip and sets a tick every second
 * to maintain the wiznet dhcp given ip
 *
 **/

#include <stdint.h>

struct Connection;

// use DEBUG_BUILD to enable debug messages

/**
 * @brief Initialise errno system and spi bus
 *
 * @return -1 and errno is set or 0
 */
int8_t SystemInit(void);

/**
 * @brief Handle dhcp state machine
 *
 *	The dhcp implementation is
 *	provided by the wiznet lib.
 *	This function uses the DHCP_run()
 *	method and switches the enum values.
 *	The connections are initialised
 *	if the ip changed.
 *
 * @param connections the connection array
 * @param end past-the-end connection ptr
 *
 * @return 0, -1 on error
 */
int8_t SystemHandleDhcp(struct Connection *connections,
			       const struct Connection *const end);

/**
 * @brief errno - Error codes
 */
enum SYSTEM_ERRNO {
	SME_NONE = 0,	 /**< no error happened*/
	SME_SOCKET_NUM = 1,  /**< invalid socket num */
	SME_SOCKET_MODE = 2, /**< invalid socket mode */
	SME_SOCKET_OPT = 3, /**< invalid socket option */
	SME_SOCKET_FLAG = 4, /**< invalid setsockopt */
	SME_SETSOCKOPT_NUM = 5,
	SME_SETSOCKOPT_OPT = 6,
	SME_SETSOCKOPT_MODE = 7,
	SME_GETSOCKOPT_NUM = 8,
	SME_GETSOCKOPT_OPT = 9,
	SME_GETSOCKOPT_MODE = 10,
	SME_LISTEN_INIT = 11,
	SME_LISTEN_CLOSED = 12,
	SME_TIMEOUT = 13,
	SME_DATALEN = 14,
	SME_MOTOR_STUCK_CLOSED_LOOP = 15,
	SME_MOTOR_STUCK_OPEN_LOOP = 16,
	SME_MOTOR_NO_MOTOR = 17,
	SME_MOTOR_ABNORMAL_KT = 18,
	SME_MOTOR_ABNORMAL_SPEED = 19,
	SME_MOTOR_LOCK_CURRENT_LIMIT = 20,
	SME_MOTOR_OVER_TEMP = 21,
	SME_MOTOR_SLEEP_STANDBY = 22,
	SME_MOTOR_OVERCURRENT = 23,
	SME_MOTOR_MTR_LOCK = 24,
	SME_MOTOR_I2C_ERROR = 25,
	SME_WIZCHIP_INIT = 26,
	SME_DHCP_IP_CONFLICT = 27,
	SME_DHCP_FAILED = 28,
	SME_SYSTEM_SETNETMODE = 29,
	SME_EVAL_NETINFO = 30,
	SME_EVAL_PHYCONF = 31,
	SME_EVAL_DHCP = 32,
	SME_SEND_STATUS = 33,
	SME_SEND_TIMEOUT = 34,
	SME_SEND_MODE = 35,
	SME_SEND_NUM = 36,
	SME_SEND_DATALEN = 37,
	SME_SEND_BUSY = 38,
	SME_RECV_STATUS = 39,
	SME_RECV_MODE = 40,
	SME_RECV_NUM = 41,
	SME_RECV_DATALEN = 42,
	SME_RECV_BUSY = 43,
	SME_CLOSE_NUM = 44,
	SME_PARSER_UNKNOWN = 45
};

/**
 * @brief Get the internal errno value
 *
 * @return the error
 */
enum SYSTEM_ERRNO SystemGetErrno(void);

/**
 * @brief Set the internal errno
 *
 * @param err error to set
 */
void SystemSetErrno(enum SYSTEM_ERRNO err);


void SystemVisualizeError(void);

#endif // SYSTEM_H_INCLUDED
