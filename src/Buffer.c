

/* Copyright (C)
 * 2016 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#include "Buffer.h"

#define NULL (void *)0

void BufferInit(struct Buffer *const b) { b->wptr = b->data; }

uint16_t BufferSize(const struct Buffer *const b) { return b->wptr - b->data; }

const uint16_t *BufferPush(struct Buffer *const b, const uint16_t *val)
{
	if (b->wptr >= b->data + BUFFER_SIZE) {
		return NULL;
	}
	*b->wptr = *val;
	return b->wptr++;
}

const uint16_t *BufferBegin(const struct Buffer *b) { return b->data; }

const uint16_t *BufferEnd(const struct Buffer *b) { return b->wptr; }

const uint16_t *BufferMemoryEnd(const struct Buffer *const b)
{
	return b->data + BUFFER_SIZE;
}

void BufferClear(struct Buffer *const b) { BufferInit(b); }
