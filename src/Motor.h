

/* Copyright (C)
 * 2016 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#ifndef MOTOR_H_INCLUDED
#define MOTOR_H_INCLUDED

#include <stdbool.h>
#include <stdint.h>

/**
 * @brief Init the motor by setting the interrupt pin
 * 	and prepare the pwm signal.
 *
 * @param ext int callback function,
 * 	is called by every motor interrupt
 */
int MotorInit(void (*motor_tick)(void));

/**
 * @brief
 *
 * @return 0 on success, else -1
 */
int MotorStart(void);

/**
 * @brief
 *
 * @return
 */
bool MotorRunning(void);

/**
 * @brief
 *
 * @return 0 on success, else -1
 */
int MotorStop(void);

/**
 * @brief
 *
 * @return
 */
uint8_t MotorFreqInHz(void);

#endif // MOTOR_H_INCLUDED
