

/* Copyright (C)
 * 2016 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#ifndef DATAACQUISITION_H_INCLUDED
#define DATAACQUISITION_H_INCLUDED

#include <stdbool.h>
#include <stdint.h>

struct Buffer;
// @TODO Change Timer to 16bit timer to Timer/Counter1

/**
 * @brief Init Timer, Buffer, internal data
 */
void DataAcquisitionInit(void);

/**
 * @brief Enable the timer 0 cmp/match interrupt, fetch data
 */
void DataAcquisitionStart(void);

/**
 * @brief callback for Motor
 */
void DataAcquisitionEOS(void);

/**
 * @brief check if the DataAcquisition is running
 *
 * @return true if running
 */
bool DataAcquisitionRunning(void);

/**
 * @brief Disable the timer 0 cmp/match
 */
void DataAcquisitionStop(void);

/**
 * @brief Get stored buffer size
 *
 * @return #stored data
 */
uint16_t DataAcquisitionSize(void);

/**
 * @brief check if internal buffer is empty
 *
 * @return true if empty
 */
bool DataAcquisitionEmpty(void);

/**
 * @brief get the internal buffer
 *
 * @return struct Buffer
 */
const struct Buffer *DataAcquisitionGetBuffer(void);

/**
 * @brief lock the internal buffer for clearing
 *
 * @note the interrupt can still write values into the buffer but mem between
 * BufferBegin() and BufferEnd() is not overwritten
 *
 * @note The buffer needs an unlock afterwards!
 */
void DataAcquisitionLockBuffer(void);

/**
 * @brief @see DataAcquisitionLockBuffer
 */
void DataAcquisitionUnlockBuffer(void);

#endif // DATAACQUISITION_H_INCLUDED
