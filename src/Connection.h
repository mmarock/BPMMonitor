

/* Copyright (C)
 * 2016 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#ifndef CONNECTION_H_INCLUDED
#define CONNECTION_H_INCLUDED

#include <stdint.h>

#include "Parser.h"

struct Connection {
	int8_t socket;
	uint16_t port;
	uint16_t interrupt;
	uint8_t rxbuffer[16];
	uint8_t *rxbuffer_ptr;
};

/**
 * @brief init the Connection
 *
 * Execute socket() and listen() system call
 *
 * @param ch the connection handler
 * @param sock the used socket
 * @param port used port for socket() call
 *
 * @return 0 if ok, else -1
 */
int8_t ConnectionInit(struct Connection *const ch, int8_t sock, uint16_t port);

/**
 * @brief Update the interrupt member
 *
 * The w5500 can generate hardware interrupts. Sadly
 * the interrupt pin is not used on the arduino, but the interrupt
 * interface delivers an easy way to react on events of multiple sockets
 * and the wizchip.
 *
 * @todo See into the docs, should I react on some wizchip interrupts?
 *
 * @param ch an array of connections
 * @param len the array size of the ch array
 */
void ConnectionUpdate(struct Connection *begin, const struct Connection *const end);

/**
 * @brief process the saved interrupt flags
 *
 * @param ch one connection you want to process
 * @param elements element array which is filled by this function
 * @param cmds_n element array size
 * @param connected number of connected connections
 *
 * @return number of written commands
 */
int8_t ConnectionProcess(struct Connection *const ch,
			 enum PARSER_ELEMENT *elements, uint8_t cmds_n,
			 int8_t *connected);
/**
 * @brief reset the socket
 *
 * @param ch the connection handler
 *
 * @return -1 if failed and errno is set, else 0
 */
int8_t ConnectionReset(struct Connection *const ch);

/**
 * @brief Send all values inside DataAcquisition
 *
 * @note to save function calls, this function sends a message before starting.
 *
 * @param ch the connection handler
 *
 * @return -1 if failed and errno is set, else the number of sent values (not
 * bytes!)
 */
int16_t ConnectionSendData(const struct Connection *const ch);

/**
 * @brief Send a single uint
 *
 * @note This does not announce the value, send a message first
 *
 * @param ch the connection handler
 * @param value the value to send
 *
 * @return
 */
int16_t ConnectionSendUInt(const struct Connection *const ch, uint16_t value);

enum MSG_TYPE { MSGT_OK = 0, MSGT_ERROR };

void ConnectionSendMessage(const struct Connection *const ch, enum MSG_TYPE mt,
			   uint16_t value);

#endif // CONNECTION_H_INCLUDED
